Budapest School Kerettanterv
============================

A BPS kerettantervének forrásfájljai. Egyrészt egy Latexben megírt szöveg `latex/src`,
másrészt a "miniszter által kiadott állami kerettanterv" xml-esített változata,
amiből kinyerjük az állami tantárgyak eredmény céljait. Ebből a kettőből jön
össze a végeredmény.

A BPS kerettanterv szabályozza, "A kerettanterv a specifikus
célkitűzés-tervezés, a tanulás, és az arra történő  reflektálás
módját írja le, vagyis a tanulás folyamatát rögzíti,
míg annak pontos tartalmában szabadságot enged."
this is an xml processor and local curriculum generator for the Hungarian
education system.

Hogyan használd?
----------------

`latex/index.pdf` fájlt javaslom, ha csak a szöveg kell. Ha forrással akarsz játszani,
akkor

```
  . env/bin/activate
  python kerettanterv.py > latex/src/eredmenyek.tex
  cd latex
  pdflatex index.tex  ; bibtex index ; pdflatex index.tex ; pdflatex index.tex
```

Valószínűleg van egy UTF-8 kötőjel karakter az eredmenyek.tex fájlban, azt kézzel kell javítani.


Állami tantárgyak
-----------------

my goal is to generate the description of subjects based in a
local curriculums (kerettanterv in Hungarian) for that I first need to
make a structured text (xml files) from the published kerettanterv.



the idea is simple: we take the local curriculum of state runned schools
(allami kerettanterv) from here: http://kerettanterv.ofi.hu/ and we generate
a new one by 1. merging subjects and 2. deleting some content


# how we got the source data
1. downloaded `doc` and `docx` files from http://kerettanterv.ofi.hu/. See the original files in `source/00_doc`
2. uploaded the the files to a gdocs document
3. downloaded the text format (Download as Plain Text File) and put in `01_txt`
folder
4. created xml files

## general format of a curriculum
the local curriculum of state run school looks like this:

* curriculum describes 3 period of schools: grade 1 to 4,
grade 5 to 8, grade 9 to 12
* there are subjects for each period
* there is a general introduction for each subject for each period
* not for every year but for every two years each subjects describes
  * goal of the subject
  * learning units to be learnt by the students
  * expected outcome (result) of the subject

We don't focus on the learning units (tematikus egyseg), because we don't need
it for the time being.

## xml file format

```
<subject name="matek">
<bevezeto>

</bevezeto>

<cel grade="1-2">
<p> lorem ipsum</p>
<p> lorem ipsum</p>
</cel>

<eredmeny grade="1-2">
<li> one expected outcome </li>
<li> other expected outcomes</li>
</eredmeny>

<cel grade="3-4">
<p> lorem ipsum</p>
<p> lorem ipsum</p>
</cel>

<eredmeny grade="3-4">
<li> one expected outcome </li>
<li> other expected outcomes</li>
</eredmeny>
</subject>
```

We basically manually annotated the text files. We don't focus on the middle
of the documents.


# process
* upload `.doc` file to google docs
* download the file as text file and put in `./source/01_text`
# the Atom helper scripts to create html lists and paragrapsh from raw text
add this to your `.atom/init.coffe`

```
atom.commands.add 'atom-text-editor', 'kerettanterv:li', ->
  return unless editor = atom.workspace.getActiveTextEditor()

  editor = atom.workspace.getActiveTextEditor()
  text = editor.getSelectedText()
  lines = text.split('\n')

  itemized = (("<li>\n \t" + line + "\n</li>") for line in lines )

  newtext = itemized.join('\n')

  selection = editor.getLastSelection()
  selection.insertText(newtext)

atom.commands.add 'atom-text-editor', 'kerettanterv:p', ->
  return unless editor = atom.workspace.getActiveTextEditor()

  editor = atom.workspace.getActiveTextEditor()
  text = editor.getSelectedText()
  lines = text.split('\n')

  itemized = (("<p>\n \t" + line + "\n</p>") for line in lines )

  newtext = itemized.join('\n')

  selection = editor.getLastSelection()
  selection.insertText(newtext)
```

also use atom-wrap-in-tag package
