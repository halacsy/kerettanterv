<subject name="enek">
<title>ÉNEK-ZENE
  B változat</title>




  <bevezeto>A zenei nevelés általános és legfőbb célja az érzelmi, értelmi és jellemnevelés, az igényes zene bemutatása és megszerettetése, kulcsot adva a tanulóknak az éneklésen, zenélésen keresztül történő megismeréshez, megértéshez.
    A közös éneklés, muzsikálás élményének megteremtésén túl, melynek révén megvalósul a befogadás és az önkifejezés, valamint az egymásra figyelés harmóniája, hangsúlyt kap a közvetlen cselekvés, alkotás, önkifejezés folyamata, mely színesíti a fantáziát, formálja az ízlést.
    A zenei jelrendszerek megismerése és alkalmazása a zene megértését, befogadását segíti. A magyar népzene, a nemzeti hagyományok, a zenetörténeti korszakok legfontosabb jellemzőinek megismerésén keresztül önálló zenei világkép alakul ki, fejlődik a kritikai képesség, a nemzeti identitástudat.
    Az 1–4. évfolyamok feladata, hogy megalapozzák a zenei nevelést, a zene megszerettetéséhez és az egyes zeneművek élményt nyújtó megismeréséhez vezető út kialakítását segítsék. A mozgással, tánccal, játékkal egybekötött csoportos éneklés, a közös muzsikálás az egymásra figyelés és a közösségformálás rendkívüli lehetőségét biztosítja. Ebben az oktatási szakaszban történik az éneklési kultúra megalapozása, a helyes testtartás és légzés, a jó fizikai állóképesség kialakítása, illetve erősítése. A ritmusjátékok, az énekléshez kapcsolódó különféle mozgások gyorsan és látványosan segítik az esztétikus, jól koordinált mozgást. A zenei emlékezet, a belső hallás fejlesztése, a zenei ismeretek elsajátítása megteremti a játékos alkotás, a zenei improvizáció feltételét. A testhangszerek, ritmushangszerek/hangszerek használata fejleszti a hangszínhallást, figyelemre, koncentrációra nevel. A zenehallgatás az auditív befogadás fejlesztésének eszköze, és a gyermeki élményvilág fontos része. A néphagyományok, népszokások a zenei anyanyelv megismerésében, a nemzeti azonosságtudat megalapozásában töltenek be fontos szerepet.
    A gyermekek csoport előtti éneklése, hangszeres előadása módot ad az egyéni készségek kibontakoztatására, a helyes önértékelés kialakítására és a kiemelkedő adottságú gyermekek fejlesztésére.
    A heti 2 óra lehetőséget biztosít arra, hogy a zenei írás-olvasás ebben a képzési szakaszban azonos súllyal szerepeljen.
    Az 5–8. évfolyamok feladata, hogy megszilárdítsák a zenei alapműveltséget. A heti 1 órában megváltozik a fejlesztési feladatok szerkezete. Az éneklésre fordítható időkeret csökken, a befogadói kompetenciák fejlesztésére és a zenehallgatásra, zeneművek megismerésére fordítható idő növekszik. Az elméleti ismeretanyag a 7–8. osztályok felé haladva folyamatosan csökken, a kottaolvasás gyakorlata fokozatosan eltolódik a felismerő kottaolvasás felé.
    A 9–10. és 11–12. osztályokban, bár énekes és generatív tevékenységekhez kapcsolódóan, de tovább folytatódik a hangsúly áthelyeződése a befogadói kompetenciák irányába.
    A zenei nevelés célja is átalakul, előtérbe kerül a zene önálló értelmezésének segítése. Cél, hogy a tanulók a zenei műalkotások megismerése révén helyesen tájékozódjanak korunk kulturális sokszínűségében, értelmezni tudják a zene különféle funkcióit, valamint a médiában és a filmművészetben betöltött szerepét. Elvárás, hogy zenei dokumentumok gyűjtésével fejlődjön rendszerezési és feldolgozási készségük, képesek legyenek önálló vélemény, értékelés megfogalmazására.
    A zenehallgatás feladata elsősorban a tájékozódás, az összefüggések, a zenei fejlődés láttatása, az önálló gyűjtőmunkára és véleményalkotásra késztetés. Szerepe a művek megismerésén, elemzésén keresztül a kultúrabefogadás szándékának erősítése.


    Általánosságban véve: a Nemzeti alaptantervben megfogalmazott fejlesztési követelmények alapján minden órának része az éneklés, megfelelő egyensúlyban a zenei írás és kottaolvasás, a zenei generatív készségek fejlesztése és fenntartása, valamint a zenehallgatás kapcsán nyert zenei ismeretek, kompetenciák, az esztétikai fogékonyság bővítése.
    A zenepedagógiai tevékenység részben a tanulók iskolában, részben az iskolán kívül szerzett zenei tapasztalataira, zenei élményeire, illetve, adott esetben, zenei gyakorlatára épül. A személyi és szervezeti adottságok feltételei szerint ösztönzi a tanulókat énekkar(ok)ban és más önszerveződő zenei együttesekben való részvételre. Támogatja a közös zenei élményeken, hangversenyeken, táncházakban való részvételt.
    A kerettantervek által előírt tartalmak a tantárgyak számára rendelkezésre álló időkeret kilencven százalékát fedik le. Egy heti két (évi 72) órás időkerettel rendelkező tantárgy kerettanterve tehát évi 7 óra szabad időkeretet biztosít a tantárgy óraszámán belül a pedagógusnak, melyet a helyi igényeknek megfelelően, a kerettanterven kívüli tantárgyi tartalommal tölthet meg.
    A táblázatokban két évre vonatkozó órakeretek vannak feltüntetve, melyek a különböző tevékenységek egymáshoz viszonyított arányát is jelzik.
    A tematikai egységek és a közműveltségi tartalmak az oktatás gyakorlatában átfedik egymást. A tagolás a Nemzeti alaptanterv fejlesztési feladatai alapján készült, a jobb áttekinthetőséget szolgálja. A fejlesztési célok a tanítás során mindig az előző ismeretanyagra, elért fejlesztésre építve, komplex módon jelennek meg.
    Az ének-zene tantárgy a Nemzeti alaptantervben meghatározott fejlesztési területek, nevelési célok közül az alábbiak megvalósításához járul hozzá hatékonyan:
    * erkölcsi nevelés (az emberi kapcsolatok normáinak és szabályainak elfogadása és a cselekvés mércéjévé tétele);
    * nemzeti öntudat, hazafias nevelés (hagyományok, ünnepek, szokások ismerete, különböző kultúrákkal való ismerkedés és azok tisztelete);
    * állampolgárságra és demokráciára nevelés (önmaguk elhelyezése a közösségben, a szabályok fontosságának megértése, a tanulótársak, a szűkebb-tágabb közösség önkéntes aktivitással történő segítése);
    * az önismeret és társas kultúra fejlesztése (kommunikációs képességek, a társakkal való együttműködés képessége, a mások iránt érzett felelősség és empátia);
    * testi és lelki egészségre nevelés (kiegyensúlyozott és harmonikus személyiség fejlesztése, a mozgással erősített testtudat, érzelmi intelligencia);
    * médiatudatosságra nevelés (választékosság, fejlett ízlésvilághoz tartozó szelekciós képesség, motiváció az értékes médiatartalmak befogadására).
    A kulcskompetenciák fejlesztésében az ének-zene tárgy az alábbiakhoz járul hozzá a maga eszközeivel:
    * esztétikai-művészeti tudatosság és kifejezőkészség;
    * anyanyelvi kommunikáció;
    * idegen nyelvi kommunikáció (5–12. osztály);
    * matematikai kompetencia;
    * digitális kompetencia (9–12. osztály);
    * szociális és állampolgári kompetencia;
    * kezdeményezőképesség és vállalkozói kompetencia;
    *  hatékony, önálló tanulás (11–12. osztály).
    </bevezeto>



  5–6. évfolyam


  Tematikai egység/ Fejlesztési cél
    Zenei reprodukció I.
  Éneklés
    Órakeret 28 óra
    Előzetes tudás
    Az alsó tagozatban megismert népzenei példák és műzenei alkotások/szemelvények ismerete. Az éneklési és generatív készségek korosztálynak megfelelő szintje.
    A tematikai egység nevelési-fejlesztési céljai
    Az éneklési készség folyamatos fejlesztése, a szép és kifejező éneklés formálása.
  Dalkincs bővítése hallás utáni és jelrendszerről történő daltanítással.
  Parlando, rubato és giusto lüktetéssel történő éneklés képességének kialakítása.
  Műdalok, műzenei idézetek stílusos megszólaltatása.
  A dalok, zenei idézetek metrumára, ritmikájára és dallamára vonatkozó megfigyelések megfogalmazása. Az alkalmazkodó ritmus.
  A többszólamú éneklés fejlesztése.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    Éneklési készség fejlesztése:
  Beéneklő gyakorlatok az óra eleji ismétlés, az alkalmazó rögzítés anyagához kapcsolódóan.
  Éneklés szöveggel, szolmizálva a–e”hangterjedelemben.
  Kifejező előadásmód, helyes frazeálás.
  Parlando, rubato és giusto dallamok éneklése.
  Alkalmazkodó ritmus.
  Kvintváltás.


  Többszólamú éneklési készség fejlesztése:
  Két-, esetleg háromszólamú bécsi klasszikus, romantikus kánonok.
  Kürtmenet, tercmenet éneklése.


  A daltanítás módszerei:
  * Hallás utáni daltanítás.
  * Daltanulás kottaképről előkészítve.


  Zenei anyag:
  Magyar népdalok:
  Válogatás régi rétegű és új stílusú népdalokból (15 magyar népdal éneklése).
  Népi tánczene.
  Jeles napok, ünnepi szokások újabb dallamai (3-4 dallam).


  Magyar történeti énekek:
  Kurucdalok, az 1848–49-es szabadságharc dalai (2-3 dallam).


  Műzenei példák (8-10 mű/idézet) a bécsi klasszicizmus és romantika korából válogatva:
  A műzene tonális zenei nyelvének megismerése, különös tekintettel a dúr-moll tonalitás kialakítására.
  Hangszerkíséretes dalok (lehetőség szerint eredeti nyelven), zenei szemelvények a bécsi klasszicizmus és romantika korából.


  Nemzeti énekeink megtanulása:
  Erkel Ferenc–Kölcsey Ferenc: Himnusz
  Egressy Béni–Vörösmarty Mihály: Szózat
    Magyar nyelv és irodalom: verbális kifejezőkészség fejlesztése.


  Történelem, társadalmi és állampolgári ismeretek: magyar történeti énekek és a magyar történelem párhuzamai.


  Erkölcstan: mű (szöveg) értelmezése erkölcsi szempontok alapján.


  Dráma és tánc: népdal-néptánc, hangszeres népzene, a tánc funkciója.


  Német nyelv:
  Dalok szövegének helyes kiejtése. Szövegértés.
    Kulcsfogalmak/ fogalmak
    Régi és új stílusú népdal, kurucdalok, Kossuth-nóták. Parlando, rubato, alkalmazkodó ritmus, kvintváltás, tercmenet.
  Dal mint műfaj, duett, kórusmű, kürtmenet, a cappella.
  Himnusz. Szózat.




  Tematikai egység/ Fejlesztési cél
    Zenei reprodukció II.
  Generatív (önállóan és/vagy csoportosan alkotó), kreatív zenei tevékenység
    Órakeret 7 óra
    Előzetes tudás
    Egy- és többszólamú ritmusgyakorlatok megszólaltatása a tanult ritmuselemekkel. A páros és páratlan tagolódású zenei formaegységek érzete és helyes hangsúlyozása: kérdés-felelet alkotása ritmussal és dallammal. Dallami improvizáció pentaton, pentachord, hexachord, hétfokú hangkészletben.
    A tematikai egység nevelési-fejlesztési céljai
    A rögtönzés képességének fejlesztése a következő zenei ismeretek felhasználásával: 6/8-os, 3/8 metrum, felütés, dúr-moll dallami fordulatok. Kürtmenet, tercpárhuzam, szekvencia szerkesztése megadott dallamhoz.
  Zenei kérdés és felelet, variáció rögtönzése ritmussal és dallammal.
  A zenei stílus- és formaérzék fejlesztése.
  A zene keltette gondolatok és érzelmek verbális kifejezése, azok zenei ihletettségű megjelenítése vizuális technikákkal.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    Generativitás fejlesztése:
  Periódus méretű, illetve két- háromtagú zenei egységekhez kapcsolódó azonosságok, hasonlóságok, különbözőségek, ismétlődés, variáció megfigyelése és tudatosítása.
  Kürtmenet, tercpárhuzam, szekvencia szerkesztése megadott dallamhoz.
  Kérdés-felelet, valamint dallami párbeszéd alkotása dúr-moll fordulatokkal (szolmizálva is), négy és nyolcütemes egységekben. Kvintváltás.
  Visszatéréses háromtagúság és rondóforma alkotása négy és nyolcütemes egységek felhasználásával.
  Megadott zenei anyagokhoz variációk (ritmikai, tempóbeli, dinamikai, dallami, karakterbeli) fűzése.
  Szimmetriát, aszimmetriát, ismétlődő, visszatérő elemeket ábrázoló képi és tárgyi alkotásokhoz dallam és ritmus társítása.


  Hallásfejlesztés:
  Hangrelációk érzékeltetése térbeli mutatással, kézjellel.
  A tanult dalokból a felemelt, illetve leszállított szolmizációs hangok, a fi, si, tá kiemelésének képessége.
  Rögtönzés dúr-moll hangnemekben.
  Átmenet nélküli vagy átmenettel történő dinamikai változások (forte, piano, crescendo, decrescendo) érzékeltetése kreatív gyakorlatokkal.
  Belső hallást fejlesztő énekes gyakorlatok: dallambújtatás, dallamelvonás.


  Zenei memória fejlesztése:
  Játékos memóriagyakorlatok megadott ritmus- és dallamfordulatokkal.


  Ritmikai készség fejlesztése:
  6/8, 3/4 és 3/8 helyes hangsúlyozása, páros-páratlan metrumok váltakozásának felismerése, reprodukciója és ezek gyakorlása kreatív feladatokkal.
  Énekléssel megismert ritmusfordulatokhoz kapcsolódó ritmusmotívumok hangoztatása ritmusnevekkel, testhangszerekkel és ritmushangszerekkel.
  Ritmus-osztinátó, ritmusolvasás, ritmusfelelgetés, ritmuspótlás, ritmuslánc, ritmus-memoriter.
  Ütemhangsúly érzékeltetése ütemezéssel.
  Többszólamú ritmusimprovizáció.
    Magyar nyelv és irodalom: mondatszerkezetek: kérdés és felelet, versek ritmusa, szótagszáma, verssorok ritmizálása.




  Vizuális kultúra: Önkifejezés, érzelmek kifejezése többféle eszközzel.
  Szimmetriát, aszimmetriát, ismétlődő, visszatérő elemeket ábrázoló alkotások.


    Kulcsfogalmak/ fogalmak
    Dúr, moll, kürtmenet, tercpárhuzam, szekvencia, kvintváltás, zenei periódus, visszatérő szerkezet, rondó, szimmetria, aszimmetria, 6/8, 3/8.




  Tematikai egység/ Fejlesztési cél
    Zenei reprodukció III.
  Felismerő kottaolvasás, zeneelméleti ismeretek
    Órakeret 10 óra
    Előzetes tudás
    Az alsó tagozatban az énekléssel és a generatív tevékenységekkel megszerzett és egyre gazdagodó ritmikai, metrikai és dallami ismeretek.
  A tanult ritmikai és dallami elemek felismerése kézjelről, betűkottáról, hangjegyről, és azok hangoztatása tanári segítséggel, csoportosan.
    A tematikai egység nevelési-fejlesztési céljai
    A zenei reprodukció fejlesztése további ritmikai, metrikai és dallami elemek elsajátításával.
  A zenei jelenségek terminológiájának megismerése és használata.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    A zeneelméleti ismeretek megszerzése az előkészítés – tudatosítás – gyakorlás/alkalmazás hármas egységében történik.


  Ritmikai elemek, metrum:
  Ritmikai elemek elnevezése, gyakorlóneve és jele: a tizenhatod (ti-ri-ri-ri/ri-ri-ri-ri) és szünetjele (sz), a tizenhatodos ritmusképletek (ri-ri-ti és ti-ri-ri), a kis éles (ri-tij/ri-tim) és kis nyújtott ritmus (tij-ri/tim-ri), a triola. A notáció helyes alkalmazása.
  Felütés, csonka ütem. A hangsúlyos és hangsúlytalan ütemegységek megkülönböztetése.
  Ütemmutatók, ütemfajták: 6/8, 3/8.


  Dallami elemek:
  Felemelt, illetve leszállított szolmizációs hangok: fi, si, tá.
  További szolmizációs hangok: r’ és m’.
  A szolmizációs hangok kézjele, betűjele, a hangjegyek elhelyezése az ötvonalas rendszerben. A notáció helyes alkalmazása.
  Kromatikus dallamfordulatok.


  Felismerő kottaolvasás:
  Tanult dallam felismerése kottaképről. Éneklés szolmizálva, szöveggel.


  Hangközök:
  Tiszta oktáv és prím, tiszta kvart és kvint, kis és nagy szekund, kis és nagy terc ismerete, intonálása, szolmizálása és felismerése hangzó anyagban és kottaképről is.


  Hangsorok, hangnemek, harmóniák:
  Dúr és moll hangsorok, vezetőhang.
  Hangnemek 1# 1b-ig, párhuzamos hangnemek.
  Dúr és moll harmónia, vezetőhang.


  További zeneelméleti ismeretek:
  Violinkulcs, abszolút hangnevek g–f” (megismertetés szintjén).
  A módosító jelek (kereszt, bé, feloldójel) használata.
  Előjegyzések, hangnemek.
  A kottában előforduló, zenei előadásra vonatkozó tempó-, dinamikai és előadási jelek értelmezése.
  A régi és új stílusú népdalok legfontosabb elemzési szempontjai (dallamvonal, sorszerkezet, kvintváltás, hangsor, szótagszám, előadásmód).
    Magyar nyelv és irodalom: jelek és jelrendszerek ismerete.


  Vizuális kultúra: térbeli tájékozódás.


  Matematika: számsorok, törtek.
    Kulcsfogalmak/ fogalmak
    6/8, 3/8, felütés, csonka ütem, tizenhatod ritmus és képletei, kis éles és kis nyújtott ritmus; triola.
  Módosító jelek, fi, si, tá módosított hangok.
  Violinkulcs, abszolút hangnevek.
  Előjegyzés, hangnem, dúr és moll hangnem, dúr és moll harmónia.
  Hangközök: tiszta oktáv és prím, tiszta kvart és kvint, kis és nagy szekund, kis és nagy terc.
  Dallamvonal, sorszerkezet, kvintváltás, hangsor, szótagszám, előadásmód.


  ________________


  Tematikai egység/ Fejlesztési cél
    Zenei befogadás I.
  A befogadói kompetenciák fejlesztése
    Órakeret 10 óra
    Előzetes tudás
    Koncentrált figyelem zenehallgatás közben, fejlett hangszínhallás, ismeretek hangszerekről.
  Többszólamú halláskészség, valamint fejlődő formaérzék.
    A tematikai egység nevelési-fejlesztési céljai
    A zeneértő és -érző képesség fejlesztése.
  A tanult zenei korszakokhoz kapcsolódó zenei formák és műfajok felismerése.
  A hangszeres együttesek, zenekarok hangzásának felismerésére való képesség fejlesztése.
  A befogadói kompetenciák fejlesztése éneklés és generatív tevékenységek kiegészítésével történik.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    A figyelem képességének fejlesztése:
  A figyelem időtartamának növelése hosszabb zenehallgatási anyag segítségével.
  Zenei megfigyelésre koncentráló feladatok.


  A hangszínhallás és a többszólamú hallás fejlesztése:
  Énekes hangfajok megkülönböztetése (szoprán, mezzo, alt, tenor, bariton, basszus).
  Hangszínhallás további fejlesztése új hangszerek megfigyeléséhez kapcsolódóan: tárogató, brácsa, cselló, nagybőgő, harsona, tuba, hárfa.
  Népi zenekar, vonósnégyes, szimfonikus zenekar hangzásának felismerése. A hangszerelés különbözőségeinek megállapítása.
  Tanult dallamok felismerése különböző hangszerek megszólaltatásában.


  Intellektuális munka:
  A formaérzék fejlesztése, zenei elemzés a következő fogalmak használatával:
  kéttagú forma, háromtagú („da capo”-s) forma, szonátaforma, triós forma, rondóforma, variációs forma.
  A formák ábrázolása formaképlettel vagy rajzzal.
  Különböző karakterek zenei ábrázolásának megfigyeltetése és jellemzése.
  A dalok szövegének értelmezése, a dalszövegekhez vagy zeneművekhez kapcsolódó dramatizált előadás (drámajáték, báb).


  A zeneirodalmi példák befogadását segítő kiegészítő ismeretek:
  A zenetörténeti korszak, a zeneszerzői életrajz megfelelő részei, a megismert zeneművek műfaja, formája, előadói apparátusa. A szimfonikus zenekar összetétele.


  Népzene:
  Jeles napok, ünnepi szokások.
  Néptánc-dialektusok, hangszeres népi tánczene.
    Magyar nyelv és irodalom: szövegértés, értelmezés.


  Dráma és tánc: tánctételek a nép- és műzenében.


  Vizuális kultúra:
  népművészeti motívumok összehasonlítása, szimmetriát, aszimmetriát, ismétlődő, visszatérő elemeket ábrázoló alkotások.


    Kulcsfogalmak/ fogalmak
    Periódus, kéttagú forma, háromtagú („da capo”-s) forma, szonátaforma, triós forma, rondóforma, variációs forma.
  Énekes hangfajok.
  Tárogató, brácsa, cselló, nagybőgő, harsona, tuba, hárfa.
  Népi zenekar, vonósnégyes, szimfonikus zenekar.




  Tematikai egység/ Fejlesztési cél
    Zenei befogadás II.
  Zenehallgatás
    Órakeret 10 óra
    Előzetes tudás
    Felismerik a tanult zenei műfajokat, formákat, hangszercsoportokat.
  Figyelemmel tudják kísérni a zenei folyamatokat, és helyesen alkalmazzák a hozzájuk kapcsolódó zenei kifejezéseket.
    A tematikai egység nevelési-fejlesztési céljai
    A művek befogadása, elemzése motivációt nyújt hangverseny-látogatásra, a kompozíciók újbóli meghallgatására.
  A népzene tárházából és a megjelölt korok zeneműveiből válogatva, a tanulók érdeklődését felkeltve, törekvés a zeneirodalom további műveinek megismerésére.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    ZENEHALLGATÁSI ANYAG
  Az osztály énekes és generatív tevékenységeihez, valamint a befogadói kompetenciák fejlesztéséhez kapcsolódóan:


  Népzene:
  A tanult népdalok, hangszeres népzene felvételei, népi hangszerek megismertetése felvételről, lehetőség szerint élőzenei bemutatásban is. Forrás:
  Kallós Archívum, Bartók Archívum, Pátria CD-rom, Magyar Népzenei Antológia DVD-rom, Magyar Népzene 1–2. (szerk. Rajeczky Benjamin), Vargyas Lajos: A magyarság népzenéje – CD melléklet 1–10. stb.


  Zeneirodalmi szemelvények a bécsi klasszicizmus és romantika korából:
  * dalok, operarészletek, oratorikus műrészletek (áriák, kórustételek);
  * hangszeres művek: szerenád, szonáta, szimfónia, vonósnégyes, versenymű, karakterdarabok, szimfonikus költemény.


  Beethoven, L. van: D-dúr hegedűverseny, III. tétel
  Beethoven, L. van: V. c-moll, „Sors” szimfónia, I. tétel
  Haydn, J.: D-dúr „Óra” szimfónia, II. tétel; G-dúr „Üstdob”/„Meglepetés” szimfónia, II. tétel
  Haydn, J.: D-dúr „Pacsirta” vonósnégyes, I. tétel (Op. 64, No.5.)
  Mozart, W. A.: Kis éji zene – I., II., III. tétel
  Mozart, W. A.: A dúr szonáta, I. tétel
  Mozart, W. A.: A varázsfuvola – részletek
  Mozart, W. A.: Requiem – részletek
  Brahms, J.: Magyar táncok – részletek
  Chopin, F.: „Esőcsepp” prelüd; mazurkák
  Erkel F.: Bánk bán; Hunyadi László – részletek
  Liszt F.: XV. Magyar rapszódia
  Liszt F.: Mazeppa
  Mendelssohn-Bartholdy, F.: e-moll hegedűverseny, I. tétel
  Muszorgszkij, M. – Ravel, M.: Egy kiállítás képei – részletek
  Schubert, F.: Gute Nacht!; Der Leiermann; Die Forelle


  A zenehallgatás történhet élő zenei bemutatással, vagy felvételről (audio, videó).
  A felsorolás a minimális követelményeket határozza meg. A zeneművek megadott listája a tanár egyéni választása alapján bővíthető. A megadott művek egy része olyan terjedelmű, hogy az ének-zene óra keretei között csak részletek meghallgatására van mód. A megfelelő részletek kiválasztásához a fejlesztési céloknál meghatározott tartalmak adnak iránymutatást.
    Magyar nyelv és irodalom: magyar történelmi énekek, irodalmi párhuzamok.


  Történelem, társadalmi és állampolgári ismeretek: történelmi korok, korstílusok.


  Erkölcstan: zeneművek erkölcsi tartalma, üzenete, emberi érzések
  széles skálája (szeretet, gyűlölet, hűség, igazság, elválás, hősiesség stb.)


  Dráma és tánc: tánctételek a nép- és műzenében.


  Vizuális kultúra: Művészettörténeti stíluskorszakok.
  Népművészeti stílusok.


  Német nyelv:
  Dalok szövegének helyes kiejtése. Szövegértés.
    Kulcsfogalmak/ fogalmak
    Dal, opera, oratórium/oratorikus művek, ária, szerenád, szonáta, szimfónia, vonósnégyes, versenymű, karakterdarab, szimfonikus költemény.




  A fejlesztés várt eredményei a két évfolyamos ciklus végén
  <eredmeny grade="5-6">
    <li>
 	A tanulók képesek 20-22 népdalt, történeti éneket több versszakkal, valamint 8-10 műzenei idézetet emlékezetből, a–e” hangterjedelemben előadni.
</li>
<li>
 	    Képesek kifejezően, egységes hangzással, tiszta intonációval énekelni, és új dalokat megfelelő előkészítést követően hallás után megtanulni.
</li>
<li>
 	    Többszólamú éneklési készségük fejlődik. Képesek csoportosan egyszerű kánonokat megszólaltatni.
</li>
<li>
 	    Kreatívan vesznek részt a generatív játékokban és feladatokban. Érzik az egyenletes lüktetést, tartják a tempót, érzékelik a tempóváltozást. A 6/8-os és 3/8-os metrumot helyesen hangsúlyozzák.
</li>
<li>
 	    Felismerik és megszólaltatják a tanult zenei elemeket (metrum, dinamikai jelzések, ritmus, dallam).
</li>
<li>
 	    Szolmizálva éneklik a tanult dalok stílusában megszerkesztett rövid dallamfordulatokat kézjelről, betűkottáról és hangjegyről. Megfelelő előkészítés után hasonló dallamfordulatokat rögtönöznek.
</li>
<li>
 	    Fejlődik zenei memóriájuk és belső hallásuk.
</li>
<li>
 	    Fejlődik formaérzékük, a formai építkezés jelenségeit felismerik és meg tudják fogalmazni.
</li>
<li>
 	    Fejlődik hangszínhallásuk. Megkülönböztetik a tárogató, brácsa, cselló, nagybőgő, harsona, tuba, hárfa hangszínét. Ismerik a hangszerek alapvető jellegzetességeit.
</li>
<li>
 	    Különbséget tesznek népi zenekar, vonósnégyes, szimfonikus zenekar hangzása között.
</li>
<li>
 	    A két zenei korszakból zenehallgatásra kiválasztott művek közül 18-20 alkotást/műrészletet ismernek.
</li> </eredmeny>




  Tárgyi feltételek:
     * Szaktanterem pianínóval vagy zongorával
     * Megfelelő nagyságú tér a mozgáshoz, népszokások előadásához
     * Ötvonalas tábla
     * Ritmushangszerek
     * Jó minőségű CD- és DVD-lejátszó, erősítő, hangszórók
     * Számítógép internetkapcsolattal, projektorral
     * Hangtár
     * Kottatár




  7–8. évfolyam


  Tematikai egység/ Fejlesztési cél
    Zenei reprodukció I.
  Éneklés
    Órakeret 25 óra
    Előzetes tudás
    A tanulók képesek népdalokat, történeti éneket, valamint műzenei idézeteket emlékezetből, a–e” hangterjedelemben több versszakkal, csoportosan előadni.
  Képesek kifejezően, egységes hangzással, tiszta intonációval énekelni, és új dalokat megfelelő előkészítést követően hallás után vagy kottaképről előkészítve megtanulni.
  Többszólamú éneklési készségük fejlődik. Képesek csoportosan egyszerű kánonokat megszólaltatni.
    A tematikai egység nevelési-fejlesztési céljai
    A dalkincs ismétlése és folyamatos bővítése. Többféle zenetörténeti stílusból válogatott szemelvények éneklése: magyar népdalok, más népek dalai, kiegészítve a magyar populáris zene műfajaiból válogatott néhány példával. Az énekhang további képzése egy- és többszólamú gyakorlatokkal, figyelve a tanulók egyéni hangi fejlődésére (mutálás). A dalok, zenei idézetek metrumára, ritmikájára és dallamára vonatkozó megfigyelések megfogalmazása.
  A többszólamú éneklés fejlesztése.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    Éneklési készség fejlesztése:
  Beéneklő gyakorlatok az óra eleji ismétlés, az alkalmazó rögzítés anyagához kapcsolódóan.
  Éneklés szöveggel, szolmizálva g–f”hangterjedelemben.
  Kifejező előadásmód, helyes frazeálás.


  Többszólamú éneklési készség fejlesztése:
  Két-, esetleg háromszólamú reneszánsz, illetve barokk művek/műrészletek/kánonok, orgánumok megszólaltatása.


  A daltanítás módszerei:
  * Hallás utáni daltanítás.
  * Daltanulás kottaképről előkészítve.


  Zenei anyag:
  Népzene:
  Válogatás régi rétegű és új stílusú népdalokból (10 magyar népdal éneklése).
  Magyar népballadák (1-2 ballada éneklése).
  Nemzetiségeink, illetve más népek dalai (2-3 dallam).


  Magyar történeti énekek:
  Históriás énekek (1-2 dallam).


  Műzenei példák (8-10 ének, idézet) a középkor, reneszánsz, barokk korból, a XX. század és napjaink zenéjéből:
  A zenehallgatási anyaghoz kapcsolódó énekes anyag: gregorián énekek, trubadúr dalok, áriák, zenei szemelvények, témarészletek a reneszánsz, barokk korból és a XX. század, illetve napjaink zenéjéből (az áriák/dalok hangszerkísérettel, lehetőség szerint eredeti nyelven).
  Népdalok 20. századi vagy mai feldolgozásai.
  Példák a populáris zenéből.
    Magyar nyelv és irodalom: régies kifejezések, szövegértés.


  Idegen nyelvek: Középkori, reneszánsz énekek, barokk művek, műrészletek, más népek dalai eredeti nyelven.
  Helyes kiejtés, szövegértés.
    Kulcsfogalmak/ fogalmak
    Népdalfeldolgozás, ballada, históriás ének, gregorián ének, trubadúr dallam.




  Tematikai egység/ Fejlesztési cél
    Zenei reprodukció II.
  Generatív (önállóan és/vagy csoportosan alkotó), kreatív zenei tevékenység
    Órakeret 10 óra
    Előzetes tudás
    Ritmus- és dallamvariálási készség, tonális és funkciós érzet, formaérzék, többszólamú alapkészségek.
    A tematikai egység nevelési-fejlesztési céljai
    A kialakított készségek továbbfejlesztése a zenei stílus- és formaérzék elsődlegességével.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    Generativitás fejlesztése:
  Aszimmetrikus, aranymetszés arányai szerint épülő, és aleatorikus zenei egységek megfigyeltetése és tudatosítása.
  Oktáv- és kvintpárhuzam szerkesztése megadott dallamhoz.
  Megadott zenei anyagokhoz variációk (ritmikai, tempóbeli, dinamikai, dallami, karakterbeli) fűzése. Augmentálás, diminuálás.
  Szimmetriát, aszimmetriát, aranymetszést, tükröződést ábrázoló képi és tárgyi alkotásokhoz dallam és ritmus társítása.


  Hallásfejlesztés:
  Megadott dallam éneklése oktáv- és kvintpárhuzamban – orgánum technika.
  Rögtönzés dúr-moll hangnemekben, szekvencia alkotása és megszólaltatása.
  Átmenet nélküli dinamikai változások (forte, piano) érzékeltetése kreatív gyakorlatokkal.
  Skálák és hangzatok szerkesztése és megszólaltatása.


  Ritmikai készség fejlesztése:
  Énekléssel megismert ritmusfordulatokhoz kapcsolódó ritmusmotívumok hangoztatása ritmusnevekkel, testhangszerekkel és ritmushangszerekkel.
  Aszimmetrikus hangsúlyozású, szinkópáló ritmusgyakorlatok alkotása és reprodukálása.
  Ritmusimprovizáció afrikai és egyéb dobok, csörgők, teakfa, ill. a felsoroltakat kiegészítő további hangszerek/eszközök használatával.
  Ritmuskánon eltérő hangszínű test- és ritmushangszerek használatával.
    Vizuális kultúra: Önkifejezés, érzelmek kifejezése többféle eszközzel.
  Tükrözés, szimmetria, aszimmetria, aranymetszés.


  Matematika:
  párhuzam, tükrözés, arányok.


    Kulcsfogalmak/ fogalmak
    Augmentáció, diminúció, aszimmetrikus ütem, aranymetszés, aleatória, oktáv- és kvintpárhuzam, orgánum.




  Tematikai egység/ Fejlesztési cél
    Zenei reprodukció III.
  Felismerő kottaolvasás, zeneelméleti ismeretek
    Órakeret 7 óra
    Előzetes tudás
    A tanult ritmikai és dallami elemek felismerése kottaképről és azok alkalmazásának és újraalkotásának képessége.
    A tematikai egység nevelési-fejlesztési céljai
    További ritmikai, metrikai és dallami elemek elsajátításával a zenei reprodukció fejlesztése. Felismerő kottaolvasási képesség fejlesztése egy-egy meghallgatott zenemű kottaképének, partitúrájának követése alapján.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    A zeneelméleti ismeretek megszerzése az előkészítés – tudatosítás – gyakorlás/alkalmazás hármas egységében történik.


  Ritmikai elemek, metrum:
  A hangsúlyos és hangsúlytalan ütemegységek megkülönböztetése az aszimmetrikus ütemekben.
  Ütemmutatók, ütemfajták: 5/8, 7/8, 8/8. Alla breve.


  Felismerő kottaolvasás:
  Tanult dallam, zenemű felismerése kottaképről, követése partitúrából.


  Hangközök:
  Kis és nagy szext, kis és nagy szeptim ismerete, intonálása, szolmizálása és felismerése hangzó anyagban és kottaképről is.


  Hangsorok, hangnemek, harmóniák:
  Dúr és moll harmónia, vezetőhang.
  Dúr és moll és modális hangsorok.
  Hangnemek 2#–2b-ig, párhuzamos hangnemek.
  Kromatika, egészhangúság.
  Atonalitás, Reihe.


  További zeneelméleti ismeretek:
  A kottában előforduló, zenei előadásra vonatkozó tempó-, dinamikai és előadási jelek értelmezése.
  XX. század és napjaink notációja.
  Dodekafon szerkesztés, aleatória.
    Matematika: törtek.
    Kulcsfogalmak/ fogalmak
    5/8, 7/8, 8/8, Alla breve. Kis és nagy szext, kis és nagy szeptim.
  Modális hangsorok, egészhangúság, atonalitás, dodekafónia, Reihe, aleatória.




  Tematikai egység/ Fejlesztési cél
    Zenei befogadás I.
  A befogadói kompetenciák fejlesztése
    Órakeret 10 óra
    Előzetes tudás
    A tanulók felismerik a tanult zenei korszakokhoz kapcsolódó zenei formákat és műfajokat.
  Képesek az énekes hangfajok, a népi és klasszikus hangszeres együttesek, a különböző hangszerek hangzásának megkülönböztetésére.
    A tematikai egység nevelési-fejlesztési céljai
    Összetettebb, hosszabb, fokozott koncentrációt igénylő zenei anyag befogadásához szükséges kompetenciák fejlesztése.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    A figyelem képességének fejlesztése:
  A figyelem időtartamának növelése összetettebb, hosszabb, fokozott koncentrációt igénylő zenei anyag segítségével.
  Zenei megfigyelésre koncentráló feladatok.


  A hangszínhallás és a többszólamú hallás fejlesztése:
  Hangszínhallás további fejlesztése új hangszerek megfigyeléséhez kapcsolódóan: cimbalom, lant, csembaló, orgona, szaxofon.
  Tanult dallamok felismerése különböző hangszerek megszólaltatásában.


  Intellektuális munka:
  Zenei elemzés a következő fogalmak használatával:
  homofon és polifon szerkesztésmód, imitáció, monotematika, konszonancia, disszonancia, szekvencia.
  Partitúrakövetés.
  A zenei formálás, arányok grafikus ábrázolása.
  Különböző karakterek zenei ábrázolásának megfigyeltetése és jellemzése.


  A zeneirodalmi példák befogadását segítő kiegészítő ismeretek:
  Zenetörténeti korszakok, zeneszerzői életrajzok.
  Gregorián zene, trubadúr költészet, reneszánsz kórusmuzsika (madrigál, motetta, mise), barokk vokális (korál, kantáta, oratórium, passió, opera) és hangszeres (szvit, concerto, concerto grosso, fúga) műfajok.
  XX. század és napjaink zenéje: impresszionizmus a festészetben és a zenében; atonalitás, dodekafónia (Reihe-technika); folklórizmus; aleatória.
  A népzene feldolgozási módjai Bartók és Kodály művészetében. A kelet-európai népek népzenéje.
  Műzene és népzene eltérő hangszerhasználata, az eltérések megfigyelése.
  A populáris dalok zenei jellemzőinek megfigyelése a következő szempontok alapján: forma, szöveg, szövegábrázolás, dallami jellemzők, ritmikai elemek, hangszerelés, dallami díszítés, különleges előadói megoldások.
  Ösztönzés ismeretszerző tevékenységre: összefüggések keresése a zenei stíluskorszakok, a történelmi események és a zeneművek között, önálló vagy csoportos gyűjtőmunka keretében, az infokommunikációs technológia (IKT) alkalmazásával.
    Magyar nyelv és irodalom: középkori egyházi és világi költészet, szövegelemzés.


  Történelem, társadalmi és állampolgári ismeretek: zeneirodalmi alkotások történelmi és művészettörténeti összefüggései.


  Idegen nyelvek: Középkori, reneszánsz énekek, barokk művek, műrészletek, más népek dalai eredeti nyelven.
  Helyes kiejtés, szövegértés.


  Földrajz: európai népek és más kontinensek népeinek zenéje.


  Vizuális kultúra: művészettörténeti és zenetörténeti stíluskorszakok találkozása.




    Kulcsfogalmak/ fogalmak
    Homofónia, polifónia, imitáció, monotematika, konszonancia, disszonancia, szekvencia. Madrigál, motetta, mise, korál, kantáta, oratórium, passió, opera, szvit, concerto, concerto grosso, fúga.
  Zenei impresszionizmus, atonalitás, dodekafónia, Reihe, aleatória, folklórizmus, populáris zene.




  Tematikai egység/ Fejlesztési cél
    Zenei befogadás II.
  Zenehallgatás
    Órakeret 13 óra
    Előzetes tudás
    A zenemű gondolati tartalmát közvetítő kifejezőeszközök átélésének és értelmezésének képessége.
  A korábban tanult jellegzetes zeneművek részleteinek felismerése.
    A tematikai egység nevelési-fejlesztési céljai
    A művek befogadása, elemzése motivációt nyújt hangverseny-látogatásra, a kompozíciók újbóli meghallgatására.
  A népzene tárházából és a megjelölt korok zeneműveiből válogatva, a tanulók érdeklődését felkeltve törekvés a zeneirodalom további műveinek megismerésére.
    Ismeretek/fejlesztési követelmények
    Kapcsolódási pontok
    ZENEHALLGATÁSI ANYAG
  Az osztály énekes és generatív tevékenységeihez, valamint a befogadói kompetenciák fejlesztéséhez kapcsolódóan:


  Népzene:
  A tanult népdalok, balladák felvételei, népi hangszerek megismertetése felvételről, lehetőség szerint élőzenei bemutatásban is. Forrás:
  Kallós Archívum, Bartók Archívum, Pátria CD-ROM, Magyar Népzenei Antológia DVD-ROM, Magyar Népzene 1–2. (szerk. Rajeczky Benjamin), Vargyas Lajos: A magyarság népzenéje – CD melléklet 1–10. stb.
  Nemzetiségeink hagyományai és néptáncai.
  Európán kívüli kultúrák népzenéje (pl. Ausztrália, Peru, Japán, afrikai országok).


  Zeneirodalmi szemelvények a középkor, reneszánsz, barokk korból, a XX. század és napjaink zenéjéből:
  A középkor és a reneszánsz zenéje: gregorián, trubadúr ének, orgánum
  Madrigálok, misék, motetták
  Bach, J. S.: János passió – részletek
  Bach, J. S.: Ein feste Burg ist unser Gott, BWV 80 – részletek
  Bach, J. S.: 3. Brandenburgi verseny, BWV 1048 – 1. és 3. tétel
  Bach, J. S.: C-dúr prelúdium és fúga, BWV 846 (Wohltemperiertes Klavier I.)
  Händel, G. F.: Messiás (Messiah), HWV 56 – részletek
  Händel, G. F.: Vízi zene vagy Tűzijáték szvit – részletek
  Purcell, H.: Artúr király (King Arthur) – részletek
  Vivaldi, A.: A négy évszak – részletek
  Debussy, C.: Gyermekkuckó – részletek
  Ravel, M.: Bolero
  Bartók B.: Concerto – részletek
  Bartók B.: A kékszakállú herceg vára – részletek
  Kodály Z.: Székelyfonó – Görög Ilona balladája
  Kodály Z.: Psalmus Hungaricus
  Kodály Z.: Kállai kettős
  Schönberg, A.: Zongoraszvit, Op. 25 – Prelúdium
  John Cage: 4’33”
  Penderecki, K.: Hirosima emlékezete
  Sáry L: Lokomotív szimfónia
  Emerson-Lake-Palmer: Egy kiállítás képei (Pictures at an Exhibition)
  Presser Gábor, Kocsák Tibor, a SzörényiBródy szerzőpáros és más magyar szerzők egy-egy zenés színpadi művének részlete, pl. István, a király


  A zenehallgatás történhet élő zenei bemutatással vagy felvételről (audio, videó).
  A felsorolás a minimális követelményeket határozza meg. A zeneművek megadott listája a tanár egyéni választása alapján bővíthető. A megadott művek egy része olyan terjedelmű, hogy az ének-zene óra keretei között csak részletek meghallgatására van mód. A megfelelő részletek kiválasztásához a fejlesztési céloknál meghatározott tartalmak adnak iránymutatást.
    Magyar nyelv és irodalom: középkori egyházi és világi költészet, szövegelemzés.


  Történelem, társadalmi és állampolgári ismeretek: zeneirodalmi alkotások történelmi és művészettörténeti összefüggései.


  Idegen nyelvek: Középkori, reneszánsz énekek, barokk művek, műrészletek, más népek dalai eredeti nyelven.
  Helyes kiejtés, szövegértés.


  Földrajz: európai népek és más kontinensek népeinek zenéje.


  Vizuális kultúra: művészettörténeti és zenetörténeti stíluskorszakok találkozása.


    Kulcsfogalmak/ fogalmak
    Gregorián, trubadúr ének, orgánum, madrigál, mise, motetta, korál, kantáta, oratórium, passió, opera, szvit, concerto, concerto grosso, fúga, jazz, pop, rock, musical.




  A fejlesztés várt eredményei a két évfolyamos ciklus végén
  <eredmeny grade="7-8">
  <li>
 	  A tanulók képesek 14-17 népdalt, balladát, históriás éneket több versszakkal, valamint 8-10 műzenei idézetet emlékezetből, g–f” hangterjedelemben előadni.
</li>
<li>
 	    Képesek kifejezően, egységes hangzással, tiszta intonációval énekelni, és új dalokat megfelelő előkészítést követően hallás után megtanulni.
</li>
<li>
 	    Többszólamú éneklési készségük fejlődik. Képesek csoportosan egyszerű orgánumokat megszólaltatni.
</li>
<li>
 	    Kreatívan vesznek részt a generatív játékokban és feladatokban. Érzik az egyenletes lüktetést, tartják a tempót, érzékelik a tempóváltozást. A 5/8-os és 7/8-os és 8/8-os metrumot helyesen hangsúlyozzák.
</li>
<li>
 	    Felismerik és megszólaltatják a tanult zenei elemeket (metrum, dinamikai jelzések, ritmus, dallam).
</li>
<li>
 	    Fejlődik zenei memóriájuk és belső hallásuk.
</li>
<li>
 	    Fejlődik formaérzékük, a formai építkezés jelenségeit felismerik és meg tudják fogalmazni.
</li>
<li>
 	    Fejlődik hangszínhallásuk. Megkülönböztetik a cimbalom, lant, csembaló, orgona, szaxofon hangszínét. Ismerik a hangszerek alapvető jellegzetességeit.
</li>
<li>
 	    A zenei korszakokból kiválasztott zeneművek közül 20-25 alkotást/műrészletet ismernek.
</li>
  </eredmeny>


  Tárgyi feltételek:
     * Szaktanterem pianínóval vagy zongorával
     * Ötvonalas tábla
     * Ritmushangszerek
     * Jó minőségű CD- és DVD-lejátszó, erősítő, hangszórók
     * Számítógép internetkapcsolattal, projektorral
     * Hangtár
     * Kottatár</subject>
