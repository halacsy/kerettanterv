﻿HON- ÉS NÉPISMERET




A hon- és népismeret tartalmazza népünk kulturális örökségére leginkább jellemző sajátosságokat, nemzeti kultúránk nagy múltú elemeit, a magyar néphagyományt. Teret biztosít azoknak az élményszerű egyéni és közösségi tevékenységeknek, amelyek a család, az otthon, a lakóhely, a szülőföld, a haza és népei megbecsüléséhez, velük való azonosuláshoz vezetnek. Segíti az egyéni, családi, közösségi, nemzeti azonosságtudat kialakítását. Megalapozza és áthatja a különböző műveltségi területeket. Rendszerezett ismeretanyagként pedig lehetőséget teremt a magyar népi kultúra értékein keresztül a saját és a különböző kultúrák, a környezet értékeit megbecsülő és védő magatartás, illetve a szociális érzékenység kialakítására.
A tanulók felfedezik, hogy a nemzedékeken át létrehozott közösségi hagyomány összeköti őket a múlttal és segít nekik eligazodni a jelenben. Felismerik, hogy az emberiség évezredek óta felhalmozódott tapasztalatai a legegyszerűbb, és éppen ezért a legfontosabb mindennapi kérdésekre adott gyakorlati válaszok tárháza. Megértik a tanulók, hogy a néphagyomány az általános emberi értékek hordozója, ezért ismerete az általános műveltséghez is szükséges.
A tantárgy megalapozza a tanulók nemzeti önismeretét, nemzettudatát, a tevékeny hazaszeretetet. Tudatosítja a tanulókban, hogy először minden népnek a saját hagyományát, nemzeti értékeit kell megismernie, hogy azután másokét is, a nemzetiségek, a szomszéd- és rokonnépek, a világ többi népének kultúráját, az egyetemes értékeket, a köztük lévő kölcsönhatást is megérthesse. Ösztönöz a szűkebb és tágabb szülőföld, a magyar nyelvterület hagyományainak és történelmi emlékeinek felfedezésére, a még emlékezetből felidézhető, vagy a még élő néphagyományok gyűjtésére. Bővíti a tanulók művelődéstörténeti ismereteit, a hagyományőrzést, népi kultúránk, nemzeti értékeink megbecsülését. Értékrendjével hozzájárul a tanulók értelmi, érzelmi, etikai és esztétikai neveléséhez, a természettel való harmonikus kapcsolatuk kialakításához és a társadalomba való beilleszkedésükhöz. 
A tanítás során – pedagógiai és néprajzi szempontok szerint kiválasztott hon- és népismereti, néprajzi forrásanyagok felhasználásával – minél több lehetőséget kell teremteni a néphagyományok élményszerű megismerésére. Törekedni kell a tanulók cselekvő és alkotó részvételére a tanulás során, hogy az érzékelésen, észlelésen, élményeken keresztül jussanak el az elvontabb ismeretekig, az összefüggések meglátásáig.




5. évfolyam


Tematikai egység/ Fejlesztési cél
	Az én világom I.
A 19-20. század fordulóján jellemző hagyományos paraszti életmód
	Órakeret 2 óra
	Előzetes tudás
	Alsó tagozatos olvasmányok a családról. 
	A tematikai egység nevelési-fejlesztési céljai
	A legszűkebb közösséghez, a családhoz, a lokális közösséghez való tartozás érzésének kialakulása, értékének tudatosítása. A megszerzett ismeretekkel a néhány emberöltővel korábbi időszakról alkotott kép tágítása, ezzel az időfogalom fejlesztése.
	Ismeretek
	Fejlesztési követelmények
	Kapcsolódási pontok
	Családunk története, családfa.
Szomszédság, rokonság.
Rokoni viszonyok, elnevezések.


Nagyszüleink, dédszüleink világa falun és városban.
Nagyszüleink, dédszüleink világának erkölcsi normái. 
A harmonikusan működő családi minták.


	Családfa készítése a rokoni viszonyok, elnevezések alkalmazásával.


Családi történetek gyűjtése, mesélése a nagyszülők, dédszülők gyermekkorából, életmódjuk jellemző elemeinek kiemelése (gazdálkodó életmód – ipari munka).


A megismert családi történetek megosztása – az önkéntesség betartásával – az osztályközösséggel.


A hagyományos paraszti vagy városi családmodell működésében az értékteremtő munka, a javakkal való ésszerű gazdálkodás meghatározó szerepének felismerése.


A felmenő családtagok, rokonsághoz tartozó személyek életének időbeni behatárolása.
	Történelem, társadalmi és állampolgári ismeretek: 
Család és lakóhely.


Erkölcstan: 
Család, otthon. Egyén és közösség.


Technika, életvitel és gyakorlat: 
A családi élet színtere, a családi otthon; különböző lakókörnyezetek jellemzői.
Rokonsági és generációs kapcsolatok a családban.
	Kulcsfogalmak/ fogalmak
	Családfa, rokonsági fok (felmenő ág: apa/anya, nagyapa/nagyanya, dédapa/dédanya, lemenő ág: gyermek, unoka, dédunoka, oldalág: testvér/báty, öcs, nővér, húg, unokatestvér, nagybácsi, nagynéni (ángy), házassági rokonság (műrokonság).
	



Tematikai egység/ Fejlesztési cél
	Az én világom II.
A 19-20. század fordulóján jellemző hagyományos paraszti életmód 
	Órakeret 2 óra
	Előzetes tudás
	Az én világom. Családismeret, családtörténet.
	A tematikai egység nevelési-fejlesztési céljai:
	A lokális ismeretek tudatosításával a szülőföld szeretetének, a helyi értékek megbecsülésének erősítése. A helyi társadalom tagoltságáról, a közösség sokszínűségéről és az ebben rejlő gazdagságról szemléletes kép kialakítása. Az időbeli tájékozódás fejlesztése a helyi nevezetességek időbeli elhelyezésével.
	Ismeretek
	Fejlesztési követelmények
	Kapcsolódási pontok
	Az én városom, falum.
A lakóhely természeti adottságai, helytörténete, néphagyományai.


A település társadalmi rétegződése (etnikum, felekezet, foglalkozás, életkor, vagyoni helyzet).


A lakóhelyhez köthető neves személyiségek, nevezetes épületek, intézmények.


Helytörténet, helyi hagyományok, nevezetességek.
A régió hon- és népismereti, néprajzi jellemzői, néphagyományai.
	A helyi hagyományok megismerése, feldolgozása különböző tevékenységekkel.


Az otthon, a lakóhely, a szülőföld, a haza megbecsüléséhez vezető egyéni és közösségi tevékenységek elsajátítása (pl. a közvetlen környezet értékeinek feltárása, az emlékhelyek gondozása.)


A lakóhely nevezetes épületeinek, a régió jeles szülötteinek történelmi korszakokhoz kötése.


	Történelem, társadalmi és állampolgári ismeretek: 
Falvak és városok.
Szegények és gazdagok világa.


Technika, életvitel és gyakorlat: Lakókörnyezetek és életmódbeli jellemzők (nagyvárosi, városi, falusi települések, természeti, épített és emberi környezet).


Természetismeret: Személyes tér. A földrajzi tér: közvetlen környezet, lakóhely, környező táj, haza.


Erkölcstan: 
Szegények és gazdagok.
A vallás funkciói. Vallási közösségek és vallási intézmények.
	Kulcsfogalmak/ fogalmak
	Helytörténet, természeti környezet, neves személy, helyi hagyomány, település, régió.
	



Tematikai egység/ Fejlesztési cél
	Találkozás a múlttal I.
A paraszti ház és háztartás, a ház népe. Népi mesterségek
	Órakeret 5 óra
	Előzetes tudás
	Családtörténet, településtörténet.
	A tematikai egység nevelési-fejlesztési céljai:
	A hagyományos paraszti életmód fontosabb elemeiben a természeti tényezők meghatározó szerepe, a környezeti feltételekhez való alkalmazkodás felismertetése. A különböző mesterségek megismerése során a szakmák megbecsülésének kialakítása.
	Ismeretek
	Fejlesztési követelmények
	Kapcsolódási pontok
	A ház történeti fejlődése.
Jurta, veremház, egysejtű ház, többosztatú ház.
A házak külső jellegzetességei: falazat, tetőszerkezet, tetőformák.


A konyha, az ételkészítés és eszközei.
Kenyérsütés, gabonaételek, burgonyaételek, vajkészítés, hurkatöltés.
Konyhai cserépedények: a főzés, sütés, tálalás, élelmiszertárolás edényei.
Fazekasmesterség.


Sarkos és párhuzamos elrendezésű szobák.
Munkasarok – szentsarok.
Tároló bútorok, ülőbútorok, asztal, ágy.
Bútorművesség.


Munkamegosztás a családon belül.
Férfi és női munkák, a gyerekek feladatai.
Napi, heti és éves munkarend.


Gyermekjátékok, a belenevelődés folyamata.
Az iskola régen és ma.
A felnőttek életét utánzó játékok, eszközös, ügyességi játékok, sportjellegű játékok.
	Az életmód és a háztípusok történeti változásainak megismerése képi és egyéb információk alapján, mindezek összefüggésének felismerése.


A természeti körülmények, a rendelkezésre álló építési anyagok és a különböző háztípusok kialakulása közötti összefüggések felfedezése.


A konyhai cserépedények funkció szerinti rendszerezése; a legfontosabb ételek alapanyagainak, elkészítési módjának, eszközkészletének megfeleltetése.


A berendezési tárgyak funkcióváltozásainak nyomon követése konkrét példák alapján.


Az évszázaddal korábbi idők családon belüli, korosztályok és nemek szerinti munkamegosztásának összehasonlítása a mai korral.


Népi játékok élményszerű elsajátítása. 
Az oktatás körülményeinek, módjának időben történt változásairól ismeretek szerzése képi és rövid szöveges információforrások alapján.
	Természetismeret: Településtípusok (tanya, falu, város), jellemző képük, a hozzájuk kötődő tevékenységek.


Vizuális kultúra: Lakóhelyhez közeli néprajzi tájegység építészeti jellegzetességei, népművészete.


Technika, életvitel és gyakorlat: 
A családi gazdálkodás, takarékosság. Családi munkamegosztás. Ételkészítési folyamatok.


Testnevelés és sport: Gyermekjátékok, népi játékok.


Ének-zene: 
Népi gyermekdalok, népi gyermekjátékok.


Történelem, társadalmi és állampolgári ismeretek: 
Gyermekek nevelése és oktatása régen.
	Kulcsfogalmak/ fogalmak
	Jurta, veremház, egysejtű ház, többosztatú ház, falazat, tetőtartó szerkezet, tetőformák, konyhai cserépedény, munkasarok, szentsarok, munkamegosztás, belenevelődés, utánzó játék, eszközös játék, sportjellegű játék.
	



Tematikai egység/ Fejlesztési cél
	Találkozás a múlttal II.
A hétköznapok rendje (táplálkozás, ruházat, életvitel)
	Órakeret
6 óra
	Előzetes tudás
	A paraszti ház és háztartás, a ház népe. Népi mesterségek.
	A tematikai egység nevelési-fejlesztési céljai:
	A gazdálkodó élet főbb területeinek megismerése során a rendelkezésre álló természeti javak ésszerű felhasználása előnyeinek, az önellátás fontosságának belátása. A közösen végzett munka előnyeinek, közösségerősítő hatásának felismertetése.
	Ismeretek
	Fejlesztési követelmények
	Kapcsolódási pontok
	A hétköznapok rendje.
A gazdálkodó ember legfontosabb munkái.
Gabonamunkák: szántás, vetés, aratás. A gabonamunkákhoz kapcsolódó szokások.
Állattartás: a szilaj-, a félszilaj- és a kezestartás.
A pásztorok ünnepei.
Kendermunkák.
Fonóbeli munkák és játékok.


Hétköznapi és ünnepi viselet.
Hétköznapi vászonruhák, a női és a férfi viselet darabjai. Gyermekviselet.
Néprajzi tájak eltérő ünnepi viselete.


Táplálkozás.
Hagyományos paraszti ételek.
Napi és heti étrend.
Téli és nyári étrend.
	Az állattartási módokban a honfoglalás előtti életmód elemeinek, illetve az életmódváltással bekövetkező változások következményeinek felismerése képi és szöveges, illetve egyéb (pl. tárgyi) források feldolgozása, rendszerezése alapján.


A hétköznapi vászonviselet elemeinek összehasonlítása a gyerekek ruházatával képek alapján. A hasonlóságok és a különbségek megfogalmazása. 
A jellegzetes táji ünnepi viseletek megismerése során a magyar népviseletek sokszínűségének felfedezése. A közösséghez tartozás külső kifejezésformájának észrevétele. 


Az egykori és a mai megjelenési formák jelentéstartalmának feltárása konkrét példák alapján.
Az önellátó életvitel meghatározó elemeiben a természettel kialakított harmonikus kapcsolat előnyeinek észrevétele.


A hagyományos paraszti táplálkozás jellemzőinek és a gyerekek étkezési szokásainak összevetése. A szembetűnő különbségek és a tovább élő táplálkozási szokások megfogalmazása, tapasztalatok megosztása.
	Történelem, társadalmi és állampolgári ismeretek: 
Nők és férfiak életmódja.
Öltözködés, divat.


Technika, életvitel és gyakorlat: 
Anyagok tulajdonságai, anyagok feldolgozása.


Erkölcstan: 
Szokások, normák szerepe, jelentősége.
	Kulcsfogalmak/ fogalmak
	Szántás, vetés, aratás, szilaj- és félszilaj állattartási mód, fonás, szövés, fonó, ing, gatya, pendely, szoknya, kötény, ünnepi viselet, böjtös nap, téli étrend, nyári étrend.
	



Tematikai egység/ Fejlesztési cél
	Hagyományos és népi (vallási) ünnepeink eredete és szokásrendje.
Jeles napok, ünnepi szokások a paraszti élet rendjében.
Társas munkák, közösségi alkalmak
	Órakeret 12 óra
	Előzetes tudás
	Az alsó tagozaton (ének-zene, magyar nyelv és irodalom, környezetismeret órák), valamint a különböző műveltségi területeken előforduló hon- és népismereti tartalmak.
	A tematikai egység nevelési-fejlesztési céljai
	A jeles napi, ünnepi szokások, az emberi élet fordulóihoz kapcsolódó népszokások, valamint a társas munkák (pl. szüret, kukoricafosztás) és más közösségi alkalmak (pl. vásár, búcsú) hagyományainak és jelentőségének felismertetése a paraszti élet rendjében. A hétköznapok és ünnepek váltakozásának, ritmusának feliemertetése, az ünnepek jelentőségének tudatosítása az egyén és a közösség életében. 
Egy-egy ünnepkör, jeles nap köszöntő vagy színjátékszerű szokásainak, valamint a társas munkákhoz, közösségi alkalmakhoz kapcsolódó szokások élményszerű, tevékenységközpontú, hagyományhű elsajátítása, különös tekintettel a helyi hagyományokra; a lakóhelyhez, a tájhoz való kötődés erősítése. 
	Ismeretek
	Fejlesztési követelmények
	Kapcsolódási pontok
	Jeles napok, ünnepi szokások a paraszti élet rendjében.
A jeles napok, ünnepi szokások fogalma, szerepe, általános jellemzői.
Előkészületek (advent, nagyböjt), munkatilalmak, jellegzetes ételek, népviselet, köszöntő és színjátékszerű szokások.


Karácsonyi ünnepkör.
Advent, az adventhez tartozó jeles napok, szokások (pl. Miklós és Luca napja, Ádám-Éva napja, karácsonyi kántálás, betlehemezés).
Karácsony napjától vízkeresztig, pl. névnapköszöntés, regölés, aprószentek napi vesszőzés, szilveszteri, újévi szokások, újévi köszöntők, vízkereszt, háromkirályjárás.


Farsang, farsangi szokások.
Farsangi ételek, bálok, szokások. Farsangi köszöntők és maszkos alakoskodások.


Iskolába toborzó szokások: Balázs- és Gergely-járás.


Nagyböjt és a húsvéti ünnepkör jeles napjai és szokásai.
Nagyböjti játékok, virágvasárnapi kiszehajtás és villőzés, a nagyhét jeles napjai. Húsvéti szokások, pl. húsvétvasárnapi zöldágjárás, húsvéti locsolkodás, hímes tojás készítése, fehérvasárnapi komatálküldés.


Májusfaállítás, pünkösd, pünkösdi szokások.
Pünkösdikirály-választás, pünkösdölés, pünkösdikirályné-járás.


A nyári napforduló ünnepe.
Szent Iván napi szokások, énekek.
Gazdasági ünnepek, társas munkák.


Az őszi jeles napokhoz, munkaalkalmakhoz kapcsolódó szokások (pl. szüret, fonó, fonóbeli játékok, tollfosztó, kukoricafosztó), közösségi alkalmak (pl. vásár, pásztorünnepek).


Az emberi élet fordulói.
A gyermek születése – hiedelmek és szokások.
Keresztelő. 
Gyermekkor, leány-, legényélet.
A lakodalom néhány jellegzetessége.
	A jeles napok, ünnepi szokások jelentőségének, közösséget megtartó szerepének, valamint az ünnepi előkészületek fontosságának felismerése a paraszti élet rendjében a konkrét szokások, ünnepek tevékenységeinek megismerése, elsajátítása során különböző (néprajzi) forrásanyagok segítségével, pl. eredeti hangzóanyag meghallgatása, filmek, fotók, ábrák megtekintése a különböző népszokásokról.


Az ünnepi szokások jellemzőinek megkülönböztetése a hétköznapok rendjétől.
A legfontosabb állandó és változó időpontú ünnepek felismerése.


A különböző jeles napokhoz, ünnepi szokásokhoz kapcsolódó – néprajzi és pedagógiai szempontok alapján kiválasztott – köszöntő és színjátékszerű szokások élményszerű, hagyományhű módon történő elsajátítása, eljátszása. A szokás kellékeinek elkészítése.


A kalendáriumi szokásokhoz kapcsolódó tevékenységek végzése: termés- (pl. lucabúza ültetés), férj- (pl. András napi böjtölés), időjárásjóslás (pl. hagymakalendárium); jeles napok, ünnepi szokások tárgyai (pl. hímes tojás készítése).


Régi fényképek gyűjtése, rajzok készítése kalendáriumi szokásokról, társas munkákról, közösségi alkalmakról.


Szóbeli, írásbeli élménybeszámolók hagyományos és új (helyi) közösségi, ünnepi alkalmakról.


Annak felismerése, hogyan igyekezett a hagyományos faluközösségben harmonikus kapcsolat kialakítására az ember a természettel, a faluközösség tagjaival; hogyan igazította a gazdasági munkákat az évszakok váltakozásához. 


A természetismeret, az időjárási megfigyelések, a népi időjárás jóslások szerepének felismerése a gazdasági évben.


Az emberi élet fordulóihoz kapcsolódó szokások néhány jellegzetességének összehasonlítása a mai korral, pl. olvasmányok, olvasmányrészletek segítségével.


Annak tudatosulása,  hogyan segítette a hagyományos faluközösség az egyént az emberi élet fordulóinál.


Egy-egy megismert népszokás tájegységhez, etnikai csoporthoz történő kötése.
	Erkölcstan: 
Vallási népszokások.


Dráma és tánc: Ismerkedés a táncillemmel, a naptári ünnepekhez kapcsolódó (helyi) népszokásokkal.


Ének-zene: 
A magyar népzene régi rétegű és új stílusú népdalai, a népi tánczene.


Mozgóképkultúra és médiaismeret: 
Részletek népszerű játékfilmből (pl. Jókai Mór–Várkonyi Zoltán: Egy magyar nábob – pünkösdikirály-választás).
	Kulcsfogalmak/ fogalmak
	Ünnep, jeles nap, böjt, advent, karácsonyi ünnepkör, karácsonyi kántálás, betlehemezés, bölcsőske, regölés, névnapi köszöntés, aprószentek napi vesszőzés, újévi köszöntők, háromkirályjárás, farsangi köszöntők, iskolába toborzó szokások, böjti játékok, kiszehajtás, villőzés, húsvét, zöldágjárás, komatálküldés, májusfaállítás, pünkösdölés, pünkösdi királynéjárás, nyári és téli napforduló, társasmunka, kaláka, aratás, szüret, fonó, tollfosztó, kukoricafosztó, vásár, pásztorünnep, keresztelő, leány-, legényélet, lakodalom.
	



Tematikai egység/ Fejlesztési cél
	Magyarok a történelmi és a mai Magyarország területén.
Néprajzi tájak, tájegységek és etnikai csoportok hon- és népismereti, néprajzi jellemzői a Kárpát-medencében és Moldvában.
A hazánkban élő nemzetiségek 
	Órakeret 5 óra
	Előzetes tudás
	Hon- és népismeret előző témái.
	A tematikai egység nevelési-fejlesztési céljai
	A magyar néphagyomány sokszínűségének és közös vonásainak megismerése a tájegységek, etnikai csoportok hon- és népismereti, néprajzi jellemzőinek bemutatásával. A saját közösség megismerése által az identitástudat, a kötődések erősítése. A földrajzi környezet, a történeti és a gazdasági tényezők hatásának felismerése a néphagyományok alakulására, konkrét táji példákon keresztül.
	Ismeretek
	Fejlesztési követelmények
	Kapcsolódási pontok
	A magyar nyelvterület néprajzi tájai, tájegységei, etnikai csoportjai, a határainkon túl élő magyarok.
* Dunántúl;
* Észak-Magyarország;
* Alföld;
* valamint a nagytájakhoz tartozó határon túli tájak (pl. Burgenland, Muravidék, Zoboralja, Mátyusföld, Bácska, Bánát, Kárpátalja)
* Erdély és Moldva.
Jellegzetes hon- és népismereti, néprajzi jellemzőik.
Példák az anyagi kultúra és a folklór köréből.


Magyarországon élő nemzetiségek.
	A határon túli magyarlakta területek, illetve a magyar nyelvterület nagy néprajzi tájainak azonosítása térképen.
A magyar nyelvterületen élő etnikai csoportok (pl. palóc, matyó, kun, székely) megnevezése.


A Magyarországon élő nemzetiségek (pl. német, szlovák, szerb, horvát, szlovén, román, roma) megnevezése.


A természeti adottságok, éghajlati viszonyok életmódra, népi építészetre gyakorolt hatásának felismerése. 


Képek gyűjtése a megismert tájakra jellemző viseletekről, népi építészetről, népművészetről, hagyományokról.


Tájékozódás a nagy tájegységek területéhez köthető világörökségekről, magyar szellemi kulturális örökségekről, nemzeti parkokról. 


	Természetismeret: 
A Kárpát-medence és hazánk nagytájai.
A népi kultúra értékei, népszokások.
A kulturális élet földrajzi alapjai (nyelvek, vallások).
A magyarsághoz kötődő világörökségi helyszínek.
A magyarság által lakott, országhatáron túli területek, tájak közös és egyedi földrajzi vonásai.
Nemzeti parkok, tájvédelmi körzetek.


Vizuális kultúra: Lakóhelyhez közeli néprajzi tájegység építészeti jellegzetességei, viselete és kézműves tevékenységei.
	Szülőföld.
A természeti környezet meghatározó szerepe, hatása a gazdálkodásra, a településszerkezetre, az építkezés módjára.


A táj jellegzetes népviselete, kézműipari tevékenysége.


Népszokások, népdalok, a népköltészet fennmaradt alkotásai (mesék, mondák).
	A tanuló saját szülőföldjének megismerése meglévő helytörténeti irodalom feldolgozásával, gyűjtésekkel.


A kistáj beillesztése a korábban megismert megfelelő nagytájba. (Nagyvárosok iskoláinak tanulói választhatják a városhoz közeli táj feldolgozását.)


Helyi kutatások igénybevétele a történeti múlt feltárásához.
Önálló gyűjtőtevékenység alkalmazása az ismeretszerzés folyamatában.


A természeti környezet befolyásoló hatásának felismerése a mesterséges környezet kialakításában.


Megismert helyi népszokások, népdalok, mesék, mondák közösség előtti bemutatása.
	Természetismeret:
Személyes tér. A földrajzi tér: közvetlen környezet, lakóhely, környező táj, haza.


Vizuális kultúra:
Lakóhelyhez közeli néprajzi tájegység építészeti jellegzetességei, viselete és kézműves tevékenységei.


Magyar nyelv és irodalom: 
Népdalok, népmesék, mondák.
	Kulcsfogalmak/ fogalmak
	Etnikai csoport, nemzeti kisebbség, nemzetiség, néprajzi táj, határainkon túl élő magyarok, a nemzeti összetartozás napja.
	



A fejlesztés várt eredményei az évfolyam végén
	A tanulók megismerik lakóhelyük, szülőföldjük természeti adottságait, hagyományos gazdasági tevékenységeit, néprajzi jellemzőit, történetének nevezetesebb eseményeit, jeles személyeit. A tanulási folyamatban kialakul az egyéni, családi, közösségi, nemzeti azonosságtudatuk.
Általános képet kapnak a hagyományos gazdálkodó életmód fontosabb területeiről, a család felépítéséről, a családon belüli munkamegosztásról. A megszerzett ismeretek birtokában képesek lesznek értelmezni a más tantárgyakban felmerülő népismereti tartalmakat.
Felfedezik a jeles napok, ünnepi szokások, az emberi élet fordulóihoz kapcsolódó népszokások, valamint a társas munkák, közösségi alkalmak hagyományainak jelentőségét, közösségmegtartó szerepüket a paraszti élet rendjében. Élményszerűen, hagyományhű módon elsajátítják egy-egy jeles nap, ünnepkör köszöntő vagy színjátékszerű szokását, valamint a társas munkák, közösségi alkalmak népszokásait és a hozzájuk kapcsolódó tevékenységeket.
Megismerik a magyar nyelvterület földrajzi-néprajzi tájainak, tájegységeinek hon- és népismereti, néprajzi jellemzőit. Világossá válik a tanulók számára, hogyan függ össze egy táj természeti adottsága a gazdasági tevékenységekkel, a népi építészettel, hogyan élt harmonikus kapcsolatban az ember a természettel.