from kerettanterv import Kerettanterv


def allResults():
    k = Kerettanterv()
    results = []
    for subject in k.subjects:
        for grade, list_of_results in subject.results.items():
            for result in list_of_results:
                results.append(result)

    results.sort()
    return results


def numberOfExpectedOutcome():
    k = Kerettanterv()
    results = []
    for subject in k.subjects:
        for grade, list_of_results in subject.results.items():
            print(subject.id, len(list_of_results))

if __name__ == "__main__":
    numberOfExpectedOutcome()
