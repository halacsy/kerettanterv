import xml.etree.ElementTree as ET
import re
from collections import defaultdict

XML_SOURCE_DIR = "./source/02_xml/"


def get_text_content(node):
    """ gets the text content of a single node """
    buffer = ""
    buffer += node.text
    for child in node:
        if(child.tag == "torol"):
            buffer += child.tail
        if(child.tag == "li"):
            buffer += "<li>"
            buffer += child.text
            buffer += "</li>"
        if(child.tag == "p"):
            buffer += "<p>"
            buffer += child.text
            buffer += "</p>"

    return buffer.strip()


def get_children_as_list(tag, node):
    result = []
    for child in node:
        if(child.tag == tag):
            result.append(get_text_content(child))
    return result


class Subject(object):
    """ egy targy 4 evre van kidolgozva egyben (van 8 eves gimnaziumra is
        de mi azzal most nem foglalkozunk)
        a negy evnek van egy bevezetoje, es ket eves ciklusban van celja
        es eredmenye
    """

    def __init__(self, period, xmlFile):
        self.period = period
        if period not in ['1-4', '5-8', '9-12']:
            raise ValueError(
                "period must be 1-4, 5-8 or 9-12 but got {}".format(period))
        elif period == "1-4":
            self.grades = ["1-2", "3-4"]
        elif period == "5-8":
            self.grades = ["5-6", "7-8"]
        else:
            self.grades = ["9-10", "11-12"]

        self.xmlFile = xmlFile
        try:
            with open("{}/{}/{}".format(XML_SOURCE_DIR, self.period, xmlFile), 'rb') as xml_file:
                dirty = xml_file.read().decode('utf-8')
            # see this https://stackoverflow.com/questions/1707890/fast-way-to-filter-illegal-xml-unicode-chars-in-python

            illegal_xml_re = re.compile(
                u'[\x00-\x08\x0b-\x1f\x7f-\x84\x86-\x9f\ud800-\udfff\ufdd0-\ufddf\ufffe-\uffff]')
            clean = illegal_xml_re.sub('', dirty)

            root = ET.fromstring(clean)
        except ET.ParseError as p:
            print("can't parse {} \n {}".format(xmlFile, p))
            raise(p)

        self.id = root.attrib['name']
        self.title = get_text_content(root.find("./title"))
        bevezeto = root.find("./bevezeto")
        if bevezeto is None:
            self.introduction = ""
            # raise ValueError("can't find bevezeto in {}".format(xmlFile))
        else:
            self.introduction = get_text_content(bevezeto)

        self.goals = {self.grades[0]: [], self.grades[1]: []}

        for node in root.findall("./cel"):
            grade = node.attrib['grade']
            if grade is None:
                raise ValueError("cel without grade in {}".format(xmlFile))
            elif grade not in self.grades:
                raise ValueError('not valid grades ({}) for this subject {} {}'.format(
                    grade, self.id, self.period))

            self.goals[grade] = get_children_as_list("p", node)

        self.results = {self.grades[0]: [], self.grades[1]: []}
        for node in root.findall("./eredmeny"):
            grade = node.attrib['grade']
            if grade is None:
                raise ValueError(
                    "eredmeny without grade in {}".format(xmlFile))
            elif grade not in self.grades:
                raise ValueError('not valid grades ({}) for this subject {} {}'.format(
                    grade, self.id, self.period))

            self.results[grade] = get_children_as_list("li", node)


FILES_1_4 = [
    '1.2.1_magyar_1-4.xml',
    '1.2.2_idnyelv_1-4.xml',
    '1.2.3_matemat_1-4_u.xml',
    '1.2.4-erkolcstan-1-4.xml',
    '1.2.5_kornyism_1-4_u.xml',
    '1.2.6.2_enek_1-4.xml',
    '1.2.7_vizkult_1-4_u.xml',
    '1.2.8_eletvitel_1-4.xml',
    '1.2.9_testnev_1-4_u-kieg.xml',
]
WELL_1_4 = [
    '1.2.4-erkolcstan-1-4.xml',
    '1.2.9_testnev_1-4_u-kieg.xml',
    '1.2.8_eletvitel_1-4.xml'
]
STEM_1_4 = [
    '1.2.3_matemat_1-4_u.xml',
    '1.2.5_kornyism_1-4_u.xml'
]

COOLT_1_4 = [
    '1.2.1_magyar_1-4.xml',
    '1.2.2_idnyelv_1-4.xml',
    '1.2.6.2_enek_1-4.xml',
    '1.2.7_vizkult_1-4_u.xml',
]

FILE_IDS_9_12 = [
    '3.2.02.1_idnyelv_9-12_g_u',
    #    '3.2.03.1_masodik_idnyelv_9-12',
    '3.2.04_matemat_9-12',
    '3.2.05_tort_tars_9-12_u',
    '3.2.06_etika_11-12_u',
    '3.2.07.2_biologia_9-12_g_b',
    '3.2.08.2_fizika_9-12_g_b',
    '3.2.09.2_kemia_9-10_g_b',
    '3.2.10_foldrajz_9-10_g_u',
    '3.2.11.2_enek_9-10',
    '3.2.12_drama_9-10',
    '3.2.13_vizkult_9-10',
    '3.2.14_media_9-10',
    '3.2.16_informat_9-12',
    '3.2.17_eletvitel_11-12_g',
    '3.2.18_testnev_9-12_u-kieg',
    '7.07_magyar_9-12_b_u'
]

WELL_9_12 = ['3.2.06_etika_11-12_u.xml',
             '3.2.16_informat_9-12.xml',
             '3.2.17_eletvitel_11-12_g.xml',
             '3.2.18_testnev_9-12_u-kieg.xml', ]
STEM_9_12 = [
    '3.2.04_matemat_9-12.xml',
    '3.2.07.2_biologia_9-12_g_b.xml',
    '3.2.08.2_fizika_9-12_g_b.xml',
    '3.2.09.2_kemia_9-10_g_b.xml',
    '3.2.10_foldrajz_9-10_g_u.xml',
]
COOLT_9_12 = ['3.2.02.1_idnyelv_9-12_g_u.xml',
              '3.2.05_tort_tars_9-12_u.xml',
              '3.2.11.2_enek_9-10.xml',
              '3.2.12_drama_9-10.xml',
              '3.2.13_vizkult_9-10.xml',
              '3.2.14_media_9-10.xml',
              '7.07_magyar_9-12_b_u.xml'
              ]


FILES_9_12 = []
# i forgot how to write map in python
for id in FILE_IDS_9_12:
    FILES_9_12.append("{}.xml".format(id))


def checkAllSubjectsAreReorganized():
    """ we mustn't forget any subject """
    for s in FILES_1_4 + FILES_5_8 + FILES_9_12:
        n = 0
        for (name, (subjects_1_4, subjects_5_8, subjects_9_12)) in OUR_SUBJECTS.items():
            if s in subjects_1_4 or s in subjects_5_8 or s in subjects_9_12:
                n += 1
        if (n > 1):
            print ("{} part of >1 subjects".format(s))
        elif (n == 0):
            print ("{} is not listed in any subjects".format(s))
        else:
            print ("{} is ok ".format(s))


class Kerettanterv_1_4(object):
    """docstring for kerettanterv."""

    def __init__(self):
        self.period = "1-4"
        self.subjects = [Subject("1-4", xml) for xml in FILES_1_4]

    def subjectIds(self):
        return [subject.id for subject in self.subjects]


FILES_5_8 = [
    '2.2.01.2_magyar_5-8_b_u.xml',
    '2.2.02.1_idnyelv_5-8.xml',
    '2.2.03_matemat_5-8_u.xml',
    '2.2.04_tort_tars_5-8_u.xml',
    '2.2.05_honism_5-6_u.xml',
    '2.2.06_erkolcstan_5-8.xml',
    '2.2.07_termism_5-6.xml',
    '2.2.08.2_biologia_7-8_b.xml',
    '2.2.09.2_fizika_7-8_b.xml',
    '2.2.10.2_kemia_7-8_b.xml',
    '2.2.11_foldrajz_7-8_u.xml',
    '2.2.12.2_enek_5-8.xml',
    '2.2.14_vizkult_5-8_u.xml',
    '2.2.15_informat_5-8.xml',
    '2.2.16_eletvitel_5-8.xml',
    '2.2.17_testnev_5-8_u-kieg.xml'
]

WELL_5_8 = [
    '2.2.06_erkolcstan_5-8.xml',
    '2.2.17_testnev_5-8_u-kieg.xml',
    '2.2.16_eletvitel_5-8.xml',
    '2.2.15_informat_5-8.xml',

]

STEM_5_8 = [
    '2.2.03_matemat_5-8_u.xml',
    '2.2.07_termism_5-6.xml',
    '2.2.08.2_biologia_7-8_b.xml',
    '2.2.09.2_fizika_7-8_b.xml',
    '2.2.10.2_kemia_7-8_b.xml',
    '2.2.11_foldrajz_7-8_u.xml',

]

COOLT_5_8 = [
    '2.2.01.2_magyar_5-8_b_u.xml',
    '2.2.02.1_idnyelv_5-8.xml',
    '2.2.04_tort_tars_5-8_u.xml',
    '2.2.05_honism_5-6_u.xml',
    '2.2.12.2_enek_5-8.xml',
    '2.2.14_vizkult_5-8_u.xml',

]

OUR_SUBJECTS = {
    'Harmónia': (WELL_1_4, WELL_5_8, WELL_9_12),
    'STEM': (STEM_1_4, STEM_5_8, STEM_9_12),
    'KULT': (COOLT_1_4, COOLT_5_8, COOLT_9_12)
}


class Kerettanterv_5_8(object):
    """docstring for kerettanterv."""

    def __init__(self):
        self.period = "5-8"
        self.subjects = [Subject("5-8", xml) for xml in FILES_5_8]

    def subjectIds(self):
        return [subject.id for subject in self.subjects]


class Kerettanterv_9_12(object):
    """docstring for kerettanterv."""

    def __init__(self):
        self.period = "9-12"
        self.subjects = [Subject("9-12", xml) for xml in FILES_9_12]

    def subjectIds(self):
        return [subject.id for subject in self.subjects]


def stats():
    k = Kerettanterv_9_12()
    for subject in k.subjects:
        print(('{}: bevezeto: {}, '
               'cel 9-10: {}, '
               'eredmeny 9-10: {},'
               ' cel 11-12: {}, '
               'eredmeny 11-12: {} ').format(subject.id,
                                             len(subject.introduction),
                                             len(subject.goals['9-10']),
                                             len(subject.results['9-10']),
                                             len(subject.goals['11-12']),
                                             len(subject.results['11-12'])
                                             ))


def printGoals(subject, grade):
    print("<h3>%s</h3>" % grade)
    for g in subject.goals[grade]:
        print ("<p> %s </p>" % g)


def retrieveIntroductions():
    for (subject, (orig_subjects_1_4, orig_subjects_5_8, orig_subjects_9_12)) in OUR_SUBJECTS.items():
        print("<h1>1-4. év</h1>")
        print("<h2><i>%s</i> tantárgy</h2>" % subject)
        subjects = [Subject("1-4", xml) for xml in orig_subjects_1_4]
        for s in subjects:
            print("<h3> forrás tantárgy: %s  </h3>" % s.title)
            print(s.introduction)
            printGoals(s, "1-2")
            printGoals(s, "3-4")

        print("<h1>5-8. év</h1>")
        print("<h2>%s</h2>" % subject)
        subjects = [Subject("5-8", xml) for xml in orig_subjects_5_8]
        for s in subjects:
            print("<h3> forrás tantárgy: %s  </h3>" % s.title)
            print(s.introduction)
            printGoals(s, "5-6")
            printGoals(s, "7-8")


def csvForAnnotation(k):
    counter = 0
    for subject in k.subjects:
        counter = 0
        for (grade, results) in subject.results.items():
            for result in results:
                counter += 1
                print("{}-{}\t{}\t{}\t{}".format(subject.id,
                                                 counter,
                                                 subject.id,
                                                 grade,
                                                 result))


def printCsvOutcomes():
    def printResultsForGrade(grade):
        # print("\\section{%s. évfolyam}" % grade)
        for (subject, (orig_subjects_1_4, orig_subjects_5_8, orig_subjects_9_12)) in OUR_SUBJECTS.items():
            # print("\\subsection{%s tantárgy}" % subject)
            if grade in (["1-2", "3-4"]):
                subjects = [Subject("1-4", xml) for xml in orig_subjects_1_4]
            elif grade in (["5-6", "7-8"]):
                subjects = [Subject("5-8", xml) for xml in orig_subjects_5_8]
            else:
                subjects = [Subject("9-12", xml) for xml in orig_subjects_9_12]

            for s in subjects:
                if (len(s.results[grade]) > 0):
                    # print("\\paragraph{%s tantárgy alapján}" % s.title.lower().capitalize())
                    # print("\\begin{itemize}")
                    for res in s.results[grade]:
                        orig_subject =  s.title.lower().capitalize().replace('\n', '').replace('\t', ' ')
                        print ("{}\t{}\t{}\t{}".format(grade, subject, orig_subject, res))

    for g in ["1-2", "3-4", "5-6", "7-8", "9-10", "11-12"]:
        printResultsForGrade(g)


def printLatexOutcomes():
    def printResultsForGrade(grade):
        print("\\section{%s. évfolyam}" % grade)
        for (subject, (orig_subjects_1_4, orig_subjects_5_8, orig_subjects_9_12)) in OUR_SUBJECTS.items():
            print("\\subsection{%s tantárgy}" % subject)
            if grade in (["1-2", "3-4"]):
                subjects = [Subject("1-4", xml) for xml in orig_subjects_1_4]
            elif grade in (["5-6", "7-8"]):
                subjects = [Subject("5-8", xml) for xml in orig_subjects_5_8]
            else:
                subjects = [Subject("9-12", xml) for xml in orig_subjects_9_12]

            for s in subjects:
                if (len(s.results[grade]) > 0):
                    print("\\paragraph{%s tantárgy alapján}" % s.title.lower().capitalize())
                    print("\\begin{itemize}")
                    for res in s.results[grade]:
                        print ("\\item %s" % res)
                    print("\\end{itemize}")
    for g in ["1-2", "3-4", "5-6", "7-8", "9-10", "11-12"]:
        printResultsForGrade(g)
import csv
class OutcomePrinter(object):
    """docstring for OutcomePrinter."""

    def __init__(self, translationFile=None, filter_fun=None):
        super(OutcomePrinter, self).__init__()
        self.filter_fun = filter_fun
        self.map = None
        self.printTranslated = True
        self.printNonTranslated = True
        if translationFile is not None:
            with open(translationFile, mode='r') as infile:
                reader = csv.reader(infile)
                self.map = {rows[0]:rows[1] for rows in reader}


    def is_this_ok(self, subject, grade, orig_subject):
        if(self.filter_fun is None):
            return True
        else:
            return self.filter_fun(subject, grade, orig_subject)

    def is_outcome_ok(self, outcome):
        return outcome != "del" and outcome != "delete"

    def subjectBegin(self, subject):
        pass

    def gradeBegin(self, subject, grade):
        pass

    def origSubjectBegin(self, subject):
        pass

    def translateOutcome(self, str):
        if(self.map and str in self.map):
            return self.map[str]
        return str

    def outcomes(self, subject, grade, orig_subject, outcomes):
        pass

    def origSubjectEnd(self, subject):
        pass

    def gradeEnd(self, subject, grade):
        pass

    def subjectEnd(self, subject):
        pass

    def streamEnd(self):
        pass

def parseOutcomes(printer):
    for (subject, (orig_subjects_1_4, orig_subjects_5_8, orig_subjects_9_12)) in OUR_SUBJECTS.items():
        printer.subjectBegin(subject)
        for grade in ["1-2", "3-4", "5-6", "7-8", "9-10", "11-12"]:
            printer.gradeBegin(subject, grade)
            if grade in (["1-2", "3-4"]):
                subjects = [Subject("1-4", xml) for xml in orig_subjects_1_4]
            elif grade in (["5-6", "7-8"]):
                subjects = [Subject("5-8", xml) for xml in orig_subjects_5_8]
            else:
                subjects = [Subject("9-12", xml) for xml in orig_subjects_9_12]

            for s in subjects:
                if (len(s.results[grade]) > 0):
                    orig_subject =  s.title.lower().capitalize().replace('\n', '').replace('\t', ' ')
                    if(printer.is_this_ok(subject, grade, orig_subject)):
                        printer.origSubjectBegin(orig_subject)
                        outcomes = []
                        for str in s.results[grade]:
                            translated = printer.translateOutcome(str)
                            if(printer.is_outcome_ok(translated)):
                                if(translated != str and printer.printTranslated):
                                    outcomes.append(translated)
                                elif (translated == str and printer.printNonTranslated):
                                    outcomes.append(translated)

                        printer.outcomes(subject, grade, orig_subject, outcomes)
                        printer.origSubjectEnd(orig_subject)
            printer.gradeEnd(subject, grade)
    printer.streamEnd()



class CsvPrinter(OutcomePrinter):
    """docstring for CsvPrinter."""

    def __init__(self, translationFile=None, filter_fun=None):
        super(CsvPrinter, self).__init__(translationFile, filter_fun)

    def outcomes(self, subject, grade, orig_subject, outcomes):
        for outcome in outcomes:
            print ('"{}", "{}", "{}", "{}"'.format(subject, grade, orig_subject, outcome))

class LatexPrinter(OutcomePrinter):
    """docstring for LatexPrinter."""

    def __init__(self, translationFile=None, filter_fun=None):
        super(LatexPrinter, self).__init__(translationFile, filter_fun)
        self.data = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        self.template_file = "outcome_table.tex"

    def outcomes(self, subject, grade, orig_subject, outcomes):
        orig_subject = orig_subject.split()[0]
        self.data[subject][grade][orig_subject].extend(outcomes)

    def streamEnd(self):
        import jinja2

        templateLoader = jinja2.FileSystemLoader(searchpath="./")
        templateEnv = jinja2.Environment(loader=templateLoader)

        latex_jinja_env = jinja2.Environment(
            block_start_string='\BLOCK{',
            block_end_string='}',
            variable_start_string='\VAR{',
            variable_end_string='}',
            comment_start_string='\#{',
            comment_end_string='}',
            line_statement_prefix='%%',
            line_comment_prefix='%#',
            trim_blocks=True,
            autoescape=False,
            loader=templateLoader)


        template = latex_jinja_env.get_template(self.template_file)
        outputText = template.render(data=self.data)  # this is where to put args to the template renderer

        print(outputText)



class SimpleStatPrinter(OutcomePrinter):
    def __init__(self):
        super(SimpleStatPrinter, self).__init__()

    def outcomes(self, subject, grade, orig_subject, outcomes):
        print(subject, grade, orig_subject, len(outcomes))


erettsegi = ['Idegen nyelv',  'Magyar nyelv és irodalom', 'Matematika',
'Történelem, társadalmi és állampolgári ismeretek']
def subject_iterator(filter=None):
    result = []
    for (subject, (orig_subjects_1_4, orig_subjects_5_8, orig_subjects_9_12)) in OUR_SUBJECTS.items():
         subjects = [Subject("1-4", xml) for xml in orig_subjects_1_4] + [Subject("5-8", xml) for xml in orig_subjects_5_8] + [Subject("9-12", xml) for xml in orig_subjects_9_12]
         for s in subjects:
             orig_subject =  s.title.lower().capitalize().replace('\n', '').replace('\t', ' ')
             for grade in s.grades:
                 for res in s.results[grade]:
                     if filter is None or filter(grade, subject, orig_subject, res):
                         result.append(
                         {"grade": grade, "subject": subject, "orig_subject": orig_subject, "outcome": res}
                         )
    return result

i = 0
def print_csv_for_translation():

    def filter(grade, subject, orig_subject, res):
        global i
        i = i + 1
        if (i  % 6 == 0):
            return (orig_subject in erettsegi)
        else:
            return False

    for res in subject_iterator(filter):
        print('"{}", "{}", "{}-{}-{}",,,'.format(res['outcome'], res['outcome'], res['grade'], res['subject'], res['orig_subject'] ) )

def print_outcomes_plain_text():


    for res in subject_iterator():
        print(res['outcome'])


def printOrigSubjects():
            for (subject, (orig_subjects_1_4, orig_subjects_5_8, orig_subjects_9_12)) in OUR_SUBJECTS.items():
                 subjects = [Subject("1-4", xml) for xml in orig_subjects_1_4] + [Subject("5-8", xml) for xml in orig_subjects_5_8] + [Subject("9-12", xml) for xml in orig_subjects_9_12]
                 for s in subjects:
                    orig_subject =  s.title.lower().capitalize().replace('\n', '').replace('\t', ' ')
                    if (orig_subject in erettsegi):
                        for res in s.results[s.grades[0]]:
                            print("{}\t{}\t{}".format(s.grades[0], orig_subject, res))
                        for res in s.results[s.grades[1]]:
                            print("{}\t{}\t{}".format(s.grades[1], orig_subject, res))

if __name__ == "__main__":
    # execute only if run as a script
    # csvForAnnotation(Kerettanterv_1_4())
    # csvForAnnotation(Kerettanterv_5_8())
    #stats()
    # checkAllSubjectsAreReorganized()
    # retrieveIntroductions()
    # printCsvOutcomes()
    #printLatexOutcomes()
    #printOrigSubjects()
    #print_csv_for_translation()
    def filterErettsegi(subject, grade, orig_subject):
        return (orig_subject in erettsegi)
    printer = LatexPrinter('./traslations.csv')
    parseOutcomes(printer)
