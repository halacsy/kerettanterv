\chapter{Tanulási út a Budapest Schoolban}

\begin{quote}
\emph{Hogyan tanulunk?}
Úgy tanulunk, hogy először célokat állítunk, majd a célokat támogató
foglalkozásokat szervezünk, és az eredményeinket megvizsgálva újabb
célokat állítunk és ezzel elölről kezdjük a folyamatot.
\end{quote}

\section{A saját tanulási célok}

Minden tanuló megfogalmazza és háromhavonta újrafogalmazza a \emph{saját tanulási céljait}: eredményeket, amelyeket el akar érni, képességeket, amelyeket fejleszteni akar, szokásokat, amelyeket ki akar alakítani.

A saját célok elfogadásakor a tanuló és a mentora a szülőkkel együtt \emph{tanulási szerződést} köt. Csak olyan célok kerülhetnek a saját célok közé, amelyek minden érintettnek biztonságosak és amelyek összhangban vannak a tantárgyi fejlesztési célokkal és eredményelvárásokkal.

Fontos megkötés, hogy a saját tanulási célok felét a \ref{sec:tantargyi_celok} fejezetben felsorolt tantárgyi tanulási célokból kell összeválogatni, a másik fele szabadon alakítható.

Az iskola tanárai a közösségek igényei, céljai alapján alakítják ki, hogy pontosan mi történik az iskolákban: mikor és milyen foglalkozások vannak, hogyan szervezik a mindennapokat.

Háromhavonta a tanárok és a tanulók megállnak, reflektálnak az elmúlt időszakra, és a tapasztalatok alapján újratervezik, újraszervezik a foglalkozások rendjét, tehát azt, hogy mikor és mit csinálnak majd a tagok az iskolában.

A mindennapi tevékenység során tapasztalt élmények, alkotások, elvégzett feladatok, kitöltött vizsgák, tehát mindaz, ami a tanulókkal történik, a tanulók portfóliójába kerül. A portfólió alapján nyomon követhető a tanuló fejlődése, és elért eredményei.

Tulajdonképpen a tanulási célok azt mutatják, hogy egy tanuló mit szeretne a portfóliójában látni három hónap vagy egy év múlva. De a portfólióba bekerül az is, amit nem terveztünk előre.

A tanulókat a mentoruk segíti a saját célok kitűzésében, a különböző választásoknál, a portfólióépítésben, a reflektálásban. Úgy is mondhatjuk, hogy a mentor kíséri a tanulót a saját tanulási útján.

\begin{quote}
Példa: Szeretek kirándulni, és izgatnak az extrém sportok is. Ezért a mentorommal való beszélgetés után azt tűztem ki célul, hogy megmászom egy hegyet. Hogy hogyan, milyen úton jutok a csúcsra, azt a tanárok segítenek kialakítani. Útközben fotózom. Időnként megállok, és újratervezem az utat az alapján, hogy még mindig oda akarok-e eljutni. Jó helyen vagyok?
Jól haladok? Minden tudásom és eszközöm megvan ahhoz, hogy a célomat elérjem?
Amikor végre felérek a csúcsra, ott is fotózom. A fotósorozat bekerül a portfóliómba.
A portfólióm akkor is bővül, ha véletlenül nem érnék fel a csúcsra, mert nem vagyok még kész, hogy megmásszam a hegyet.
\end{quote}

\begin{table}
\centering
\begin{tabular}{@{}p{2cm}|p{4cm}|p{4cm}@{}}

& \textbf{Hagyományos}  & \textbf{ Személyre szabott}
\\ \hline
  Mi alapján döntjük el, hogy mit tanulunk? &
  Nemzeti tantervek, következő iskolák igénye &
  Tanulók saját céljai, érdeklődése és a tantárgyi alapú fejlesztési célok egyensúlya \\
\hline
  A tanulási folyamat célja &
   Mindenkinek közös, a következő intézményre felkészítés fő célként jelenik meg &
   Mindenki saját érdeklődése és célja alapján \\
\hline
   Visszajelzés alapja &
   Az álami kerettantervhez viszonyított tudás mérése &
   A saját célokhoz viszonyítva hol tart (elérte, még nem érte el)
   Közösség többi tagjától kapott visszajelzés.  \\
\hline
  Tantárgyak napi ritmusának kialakítása &
   Előre meghatározott tanmenet alapján, előre történik &
   Két-három havonta újratervezve a célok és az elért eredmények alapján   \\

\end{tabular}
\caption{A hagyományos iskolák és a személyre szabott tanulási környezetek megközelítése teljesen eltér egymástól}

\label{tbl:szemelyreszabott}

\end{table}

A személyre szabott tanulás két dologban is eltér a hagyományos modelltől: a hagyományos iskolamodellben mindenki ugyanazért a kimenetért dolgozik, és mindenki mindent ugyanakkor és ugyanúgy tanul, csinál. Ez az, amikor mindent egy kaptafára húzunk, a nemzetközi szakirodalomban \emph{one size fits all education} címkézik ezt a megközelítést.

A hagyományos iskolák következő szinjén kialakult a „minden gyerek más" elv mentén az egyéni utak, a differenciálás fogalma. Bár mindenkinek hasonló a célja, a tanárok differenciáltan próbálják átadni a tudást, odafigyelve az egyéni igényekre.

A Budapest School személyre szabott tanulásszervezésének jellegzetessége, hogy a tanulók a saját egyéni céljuk irányába haladnak, az ahhoz a célhoz az adott kontextusban leghatékonyabb úton. Tehát mindenki rendelkezik egyéni célokkal, még akkor is, ha egy közösség tagjainak céljai között 80\% átfedés is van.

A NAT műveltségi területeiben megfogalmazott követelmények teljesítése is célja a tanulásnak, a tanulás fő irányítója azonban más. Mi azt kérdezzük a tanulóktól, hogy \emph{ezen felül} mi az ő személyes céljuk.

\section{A tanulási szerződés}


A tanulási szerződés az előbbiekben említett tanuló-mentor-szülő közötti megállapodás, ami rögzíti
\begin{enumerate}
\item A tanuló, a mentor (iskola) és a szülő igényeit, elvárásait

    Ezek lehetnek: \emph{,,szeretném, ha a gyerekem naponta olvasna''} típusú folyamatra vonatkozó kérések, vagy erősebb \emph{,,változtatnod kell a viselkedéseden, ha közösségben akarsz maradni''} igények, határok megfogalmazása.

\item A tanuló céljait a következő trimeszterre, vagy a tanév végéig

\item A tanuló, mentorok (iskola) és szülő vállalásait, amivel támogatják a cél elérését és a felek igényének elérését

\end{enumerate}

\begin{quote}
Példa: egy 7 éves gyerek, Elemér szeretne önállóbban olvasni, és ennek a szülők és tanárok is örülnek. Megállapodnak, hogy a cél, hogy a trimeszter végére a gyermek képes legyen egy minimum 20 oldalas könyvet önállóan elolvasni. Ezért a mentor vállalja, hogy társaival együtt olvasó klubot szervez, amikor hetente minimum 30 percet egyénileg olvasnak a gyerekek.
A szülők vállalják, hogy megvásárolják vagy könyvtárból kikölcsönöznek Elemérnek 3 könyvet a trimeszter során. Elemér pedig vállalja, hogy hetente 30 percet önállóan olvas.
\end{quote}

A tanulási szerződésre jellemző, hogy
\begin{itemize}
\item A kitűzött célokat minél specifikusabban, mérhetőbben kell megfogalmazni. Javasolt az OKR,  (Objectives and Key Results, azaz  Cél és Kulcs Eredmények)\cite{okr} vagy a SMART (Specific, Measurable, Achievable, Relevant, Time-bound, Specifikus, azaz Mérhető, Elérhető, Releváns és Időhöz kötött)\cite{wiki:smart} technika alkalmazása, hogy minél specifikusabb, teljesíthetőbb, tervezhetőbb és könnyen mérhető célokat tűzzenek ki.

\item A kitűzött célokban való megállapodást követően, megállapodást  kell kötni arról is, hogy ki és mit tesz azért, hogy a tanuló a célokat elérje.

\item A mentor a teljes mikroiskolát (más tanárokat, közösséget) képviseli a megállapodás során.
\end{itemize}

A tanulási szerződést néha hívjuk \emph{megállapodásnak} is. A megállapodás és szerződés szavakat ez a kerettanterv szinonímának tekinti. A \emph{lerning contract} az önirányított tanulást hangsúlyozó felnőttképzés irodalomban
bevett szakkifejezés már a 80-as évektől\cite{Malcolm77}. Ennek a magyar nyelvben inkább a szerződés felel meg. Másik szakterületen, a pszichoterápiás munkában a terápiás szerződések megkötése, azaz a közös munka kereteinek közös kialakítását hangsúlyozzák.\cite{pszichoterapia} Erre is utalunk, amikor bevezetjük a tanulási szerződés kifejezést.

 Használatos még a \emph{hármas szerződés} kifejezés is, hangsúlyozva, hogy mind a három szereplőnek elfogadhatónak kell tartania a szerződés tartalmát.

Tekintettel arra, hogy a kiskorú mellett minden esetben a szülő (törvényes képviselő) is aláírója a szerződésnek, a Ptk. 2:14 .§ (1) és (3) bekezdése szerint a jognyilatkozat érvényesen megtehető, a szerződés érvényesen létrejöhet.

\section{Moduláris tanmenet}

Az iskola fő célja, hogy képes legyen alkalmazkodni a menet közben felmerülő tanulási igényekhez, és a tanulók saját céljai felé vezető egyéni tanulási útjait minél rugalmasabban meg tudja szervezni. Ennek támogatására a tanulást modulokra, rövid tanulási egységekre bontjuk, és egy olyan modulokból álló rendszert kínálunk, amely egyszerre ad struktúrát a tanulásnak, és teszi átláthatóvá, ugyanakkor rugalmasságot is ad arra, hogy a társadalmi, az egyéni és a közösségi célok harmóniába kerülhessenek.

A modulok a tanulásszervezés egységei: olyan foglalkozások megtervezett sorozata, amelyek során egy meghatározott időn belül a tanulók valamely képességüket fejlesztik, valamilyen ismeretet elsajátítanak, és valamilyen produktumot létrehoznak. A modulok célja sokféle lehet, de a legfontosabb, hogy a résztvevők a portfóliójukba bejegyzésre érdemes eredményt hozzanak létre. Modulokba szerveződve az iskola tanulói tudnak például:
\begin{itemize}
\item Produktum létrehozására szerveződő projektben részt venni

\item Felfedezni, feltalálni, kutatni, vizsgálni, azaz kérdésekre választ keresni

\item Egy jelenséget több nézőpontból megismerni

\item Egy képességüket, készségüket fejleszteni

\item Adott vizsgára felkészülni

\item Társas programokban részt venni

\item Az önismeretükkel, a tudatosságukkal foglalkozni.
\end{itemize}

Egy-egy modul hossza és foglalkozásainak gyakorisága változó lehet: az egyszeri alkalomtól a két hónapig tartó, heti 3-5 foglalkozást magában foglaló modul is lehetséges. Egy trimeszternél azonban nem lehet hosszabb, és a lezárását az értékelés és az elért eredmények portfólióba emelése követi.

A Budapest School tanulói a felkínált modulokból maguk választják napi- és heti rendjük tartalmát, a mentoraik és szüleik segítségével.

A modulok különböznek nemcsak témájukban és céljaikban, hanem módszertanukban, folyamataikban is: bizonyos modulokban a felfedeztető (inquiry based) módszer, másik modulokban az ismétlő (repetitív) gyakorlás a célravezető. Így mindig a modul céljához, a tanárok és a tanulók képességeihez és igényeihez választható a legjobb módszer.

Modulonként változik, hogy a folyamatot a tanulók vagy a tanárok befolyásolják-e, és milyen mértékben. Két szélsőség:
\begin{enumerate}
\item Egy digitális kézműves modul célja, hogy építsünk valamit, ami programozható. Annak kitalálása, hogy mit és hogyan építünk, a tanulók feladata. Itt a modul vezetője csak támogatja a tanulás folyamatát, azaz \emph{facilitál}.

\item Egy „A vizuális kommunikáció fejlődése a XX. század második felében" modul esetén a tanár előre eldönti, hogy mely alkotók, alkotások tartoznak szerinte a mindenképpen említendők sorába (a kánonba), és ezeket sorban végigveszi a tanulókkal, még akkor is, ha minden alkotás megismerése után szabadon beszélgetnek a tanulók.
\end{enumerate}

\subsection{ Modulok megtervezése}

A modulok kiválasztása, kidolgozása, felkínálása az iskola tanárainak feladata és felelőssége. A tanárok figyelnek és reagálnak a tanulók, szülők céljaira, igényeire. A modulokat vagy maguk tartják, vagy külsős embereket hívnak meg (vagy online történik az egész). A tanárok felelőssége, hogy amikor egy tanuló moduljai befejeződnek, és újat tud felvenni, akkor az érdeklődési körének megfelelő témák közül választhasson.


\begin{table}[h]
\centering
\begin{tabular}{@{}p{1.5cm}|p{4cm}|p{4cm}@{}}

& \textbf{Hagyományos tantárgyi tanulás}  & \textbf{ Budapest School modulok}
\\ \hline
   Hossza &
   Minimum fél éves, de inkább egy éves tanulási blokkok fő &
   Lehet egy hetes is, vagy legfeljebb egy trimeszter hosszú. A lezárását követően egy új modulban tovább folytatódhat \\ \hline
Tervezése & A kimeneti követelmények alapján előre megtervezett &
 A tanulók céljai és érdeklődése alapján, akár az igény megjelenése utáni héten is indulhat\\ \hline

\end{tabular}
\caption{A hagyományos iskolák tantárgyi órái és a Budapest School moduljai struktúrálisan eltérnek egymástól.}

\label{tanorak-vs-modulok}

\end{table}



\subsection{ Modulok helyszíne}




A tanulás az egyes mikroiskolák helyszínén, vagy más Budapest School mikroiskolákban, vagy a tanár által kiválasztott külső helyszínen, esetenként pedig online, virtuális térben történik. A tanulásra, mint az élethez szorosan kapcsolódó holisztikus fejlődési igényre tekintünk, melynek jegyében az elsődleges szocializációs formától, a szülői, családi környezettől sem akarjuk a tanulást leválasztani. Az élethosszig tartó tanulás jegyében a tanulás tere az iskolai időszak után és az iskola terein kívül is folytatódik.

A tanulók több ok miatt is tanulnak az iskolán kívül:
\begin{enumerate}
\item Modulok szervezhetők külső helyszínekre, úgymint múzeumokba, erdei iskolákba, parkokba, vállalatokhoz, vagy tölthetik az idejüket „kint a társadalomban".

\item Az önvezérelt tanulás okán a tanulók képesek otthon vagy külső helyszíneken is fejlődni.
\end{enumerate}
Személyre szabott oktatás lévén a jelen kerettanterv alapján működő iskolákban a mentor, a szülő és a gyerek közös megállapodásán alapuló hiányzás mindaddig elfogadott, amíg ez a tanulónak az  egyéni megállapodásában rögzített saját céljainak elérését nem veszélyezteti, és tudomásunk van arról, hogy a folyamatos fejlődése biztosított.

\section{Portfólió}


A modulok végeztével portfóliót állítunk össze, hogy a tanulás mintázatait észlelhessük, és tudatosabban tudjuk a gyereket segíteni a céljai kitalálásában és elérésében. A portfólió a gyerek céljainak nyomonkövetését szolgálja, és egyúttal a szülői visszajelzés eszköze is. Minden tanulónak folyamatosan épül a portfóliója: ez tartalmazza az általa elvégzett feladatokat, projekteket vagy azok dokumentációját, alkotásait, eredményeit, az esetleges vizsgák eredményeit és a visszajelzéseket társaitól, tanáraitól. A \emph{portfólió célja}, hogy minden információ meglegyen ahhoz, hogy:

\begin{itemize}
\item A tanuló és mentora fel tudja mérni, hogy sikerült-e a kitűzött célokat elérni, illetve mire van szüksége még a tanulónak új célok eléréséhez

\item A szülő folyamatosan rálásson gyereke tanulási útjára

\item Megítélhető legyen, hogy a tantárgyi fejlesztési területekhez képest hol tart a tanuló

\item A gyerek a portfólió megtekintésével visszaemlékezhessen a tanultakra, ismételhessen, tudása elmélyülhessen

\item Eredményei alapján bizonyítványt lehessen kiállítani

\end{itemize}
A portfólió alakításának eseményei
\begin{enumerate}
\item Minden \emph{modul végeztével} a portfólióba bekerül:

\begin{enumerate}

\item  A készség elsajátításának ténye (mastery based learning, nálunk nincs félig elsajátított képesség), ha a modul célja például a 20-as számkörben való biztonságos összeadás és kivonás volt. Amennyiben a készséget a gyerek és a tanár megítélése alapján nem sikerült megfelelően elsajátítani, úgy a gyakorlás ténye kerül be a portfólióba.

\item Az alkotás vagy a projektmunka eredménye, ha a modul célja egy alkotás létrehozása volt.

\item A részvétel ténye, ha a jelenlét volt a modul célja (például kirándulás az Országos Kéktúra útvonalán).


\end{enumerate}
\item Az elvégzett vizsgák, tudáspróbák, képességfelmérők, diagnózisok eredményei.

\item A kipakolás, amelynek célja, hogy a tanulók a tanároknak, szülőknek és más érintetteknek bemutassák a két kipakolás között elvégzett munkájukat, azaz a portfólióváltozásukat. A kipakolásra való felkészülés tulajdonképpen a portfólió összeállítása, prezentálásra való felkészítése, a \emph{portfólió frissítése}.

\item Társas visszajelzés eredményeként minden tanuló kap visszajelzést a társaitól. Ilyenkor összegyűjtik mit tett a tanuló, ami a többiek elismerését és háláját kivívta.

\item A tanuló saját értékelése, reflexiója arról, hogyan értékeli, amit elért.

\item A tanárok folyamatosan adnak kompetenciatanúsítványokat. Ezek rövid, specifikus visszajelzések, amelyek mutatják, ha valamit a tanuló megcsinált, valamiben fejlődött.
\end{enumerate}

A mentorok segítenek a tanulás módját és eredményeit bemutató portfólió összeállításában, és abban, hogy olyan tevékenységeket szerveznek nekik, amelyek segítik őket céljaik elérésében.
